﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.ColdChainDelivery.Utilities.DynamicData.Attributes
{
    /// <summary>
    /// ValidationTypeEnum
    /// </summary>
    public enum ValidationTypeEnum
    {
        /// <summary>
        /// 數字限定
        /// </summary>
        NumericOnly,

        /// <summary>
        /// 身分證
        /// </summary>
        IdentityId,

        /// <summary>
        /// 統一編號
        /// </summary>
        CompanyNo,

        /// <summary>
        /// 手機
        /// </summary>
        Mobile,

        /// <summary>
        /// 電話
        /// </summary>
        Phone
    }
}
