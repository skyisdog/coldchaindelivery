﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.ColdChainDelivery.Utilities.DynamicData.Attributes
{
    /// <summary>
    /// Query
    /// </summary>
    [System.AttributeUsage(AttributeTargets.Property)]
    public class QueryAttribute : System.Attribute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="QueryAttribute"/> class.
        /// </summary>
        public QueryAttribute()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="QueryAttribute"/> class.
        /// </summary>
        /// <param name="data">請以 text:value;text:value ....  方式填值</param>
        public QueryAttribute(string queryColumnName, QueryAttributeOperatorEnum queryOperator)
        {
            this.QueryColumnName = queryColumnName;

            this.QueryOperator = queryOperator;
        }

        /// <summary>
        /// 查詢欄位
        /// </summary>
        public string QueryColumnName { get; set; }

        /// <summary>
        ///  關係運算子
        /// </summary>
        public QueryAttributeOperatorEnum QueryOperator { get; set; }
    }
}
