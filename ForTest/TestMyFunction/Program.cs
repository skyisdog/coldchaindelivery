﻿using System;
using System.Collections.Generic;
using LightYear.ColdChainDelivery.DA.LightYearTest;
using LightYear.ColdChainDelivery.DA.Repositories.StudentTest;
using LightYear.ColdChainDelivery.BL.Services.StudentTest;
using AutoMapper;
using LightYear.ColdChainDelivery.Mappers.Students;
using LightYear.ColdChainDelivery.BL.BE.StudentTest;

namespace TestMyFunction
{
    class Program
    {
        static void Main(string[] args)
        {
            //JunFuDbContext junFuDbContext = new JunFuDbContext();

            //DeliveryRequestRepository deliveryRequestRepository = new DeliveryRequestRepository(junFuDbContext);

            //string checkNumber = "8440893005";

            //double collectionMoney = deliveryRequestRepository.GetCollectionMoney(checkNumber);

            //Console.WriteLine(string.Format("{0} collection money is {1}", checkNumber, collectionMoney));

            // /* //在command line 測試寫入測試資料表
            var config = new MapperConfiguration(cfg => { cfg.AddProfile<StudentMappingProfile>(); });

            var mapper = config.CreateMapper();

            Console.WriteLine("Hello World!");

            var rand = new Random();

            int myTest = rand.Next(1000, 9999);

            int left = myTest % 100;

            Student student = new Student();

            student.Name = string.Format("Max Test{0}" , myTest);

            student.Age = left;

            student.Sex = (left % 2);

            LightYearTestDbContext lightYearTestDbContext = new LightYearTestDbContext();

            StudentRepository studentRepository = new StudentRepository(lightYearTestDbContext);

            int studentId =  studentRepository.Insert(student);

            Console.WriteLine(string.Format("inserted id is {0}", studentId));

            StudentService studentService = new StudentService(studentRepository, mapper);

            List<PersonEntity> students = studentService.GetAllStudents();

            foreach (PersonEntity student1 in students)
            {
                Console.WriteLine(string.Format("student name is {0}", student1.Name));
            }
            // */
        }
    }
}
