﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using Microsoft.AspNetCore.Http;
using LightYear.ColdChainDelivery;
using LightYear.ColdChainDelivery.DA.ColdChainDeliveryDb;

namespace LightYear.ColdChainDelivery.BL.BE.DeliveryRequest
{
    public class CreateDeliveryRequestEntity
    {
        public long MemberId { get; set; }
        public string MemberName { get; set; }
        public string ProjectNo { get; set; }
        public string DeliveryNumber { get; set; }
        public string OrderNumber { get; set; }
        public string ReceiverName { get; set; }
        public string ReceiverTel1 { get; set; }
        public string ReceiverTel2 { get; set; }
        public long ReceiverCityAreaId { get; set; }
        public long ReceiverZip { get; set; }
        public string ReceiverAddress { get; set; }
        public int CollectionAmount { get; set; }
        public string SenderName { get; set; }
        public long SenderCityAreaId { get; set; }
        public long SenderZip { get; set; }
        public string SenderAddress { get; set; }
        public long CreateMemberId { get; set; }

        public string SenderFullAddress { get; set; }
        public string ReceiverFullAddress { get; set; }

    }

    public class CityAreaEntity
    {
        [JsonPropertyName("city_index")]
        public long CityIndex { get; set; }
        [JsonPropertyName("city")]
        public string City { get; set; }
        [JsonPropertyName("areaEntities")]
        public AreaEntity[] AreaEntities { get; set; }
    }

    public class AreaEntity
    {
        [JsonPropertyName("id")]
        public long Id { get; set; }
        [JsonPropertyName("area")]
        public string Area { get; set; }
    }

    public class MemberEntity
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }

    public class SimpleDeliveryRequestEntity
    {
        public string DeliveryNumber { get; set; }
        public string OrderNumber { get; set; }
        public string ReceiverName { get; set; }
        public string ReceiverCityArea { get; set; }
        public string Status { get; set; }
        [JsonIgnore]
        public long ReceiverCityAreaId { get; set; }
    }

    public class DeliveryRequestInputEntity
    {
        public string DeliveryNumber { get; set; }
        public string OrderNumber { get; set; }
        public string ProjectNo { get; set; }
        public DateTime CreateDateStart { get; set; }
        public DateTime CreateDateEnd { get; set; }
        public DateTime DeliveryDateStart { get; set; }
        public DateTime DeliveryDateEnd { get; set; }
        public string ReceiverName { get; set; }
    }

    public class DeliveryRequestEntity
    {
        public long Id { get; set; }
        public string DeliveryNumber { get; set; }
        public string OrderNumber { get; set; }
        public string ProjectNo { get; set; }
        public string ReceiverName { get; set; }
        public long ReceiverCityIndex { get; set; }
        public long ReceiverCityAreaId { get; set; }
        public string ReceiverAddress { get; set; }
        public string ReceiverFullAddress { get; set; }
        public int CollectionAmount { get; set; }
        public string Status { get; set; }
        public bool IsAllowEdit { get; set; }      //是否已被司機取走，已取走則不允許修改
        public bool IsDelivery { get; set; }        //是否已配達

        //以下為僅存於整批匯入結果頁面屬性
        public string SenderName { get; set; }
        public long SenderCityIndex { get; set; }
        public long SenderCityAreaId { get; set; }
        public string SenderAddress { get; set; }
        public string SenderFullAddress { get; set; }
    }

    public class UploadExcelModel
    {
        public IFormFile File { get; set; }
        public Guid UniqueId { get; set; }
        public string ProjectNo { get; set; }
        public long MemberId { get; set; }
    }

    public class BatchExcelCellEntity
    {
        public string CellPosition { get; set; }
        public string Content { get; set; }
    }

    public class BatchDeliveryRequest
    {
        public BatchDeliveryRequest()
        {
            this.IsValidate = true;
        }
        public int Row { get; set; }        //記錄在Excel中第幾筆
        public DA.ColdChainDeliveryDb.DeliveryRequest DeliveryRequest { get; set; }
        public bool IsValidate { get; set; }
        public string ErrMsg { get; set; }
    }

    public class BatchDeliveryRequestForOutputEntity
    {
        public int Row { get; set; }        //記錄在Excel中第幾筆
        public DeliveryRequestEntity DeliveryRequestEntity { get; set; }
        public bool IsValidate { get; set; }
        public string ErrMsg { get; set; }
    }

    public class CityAreaIdAddressEntity
    {
        public long CityAreaId { get; set; }
        public string Address { get; set; }
        public string ErrMsg { get; set; }
    }

    public class BatchDeliveryRequestResultEntity
    { 
        public BatchDeliveryRequestForOutputEntity[] BatchRequests { get; set; }
        public string ExcelErrMsg { get; set; }
    }

    public class IndexParamsEntity
    { 
        public string Url { get; set; }
        public bool IsLogin { get; set; }
    }
}