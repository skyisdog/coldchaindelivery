﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace LightYear.ColdChainDelivery.BL.BE.Account
{
    public class UserInfoEntity
    {
        public long Id { get; set; }

        public string Account { get; set; }

        public string UserName { get; set; }

        public string AccountType { get; set; }

        public DateTime CookieExpiration { get; set; }

        public int LogisticId { get; set; }
    }

    public enum AccountType
    {
        管理者 = 1,

        客戶 = 2,

        司機 = 3
    }
}
