﻿using LightYear.ColdChainDelivery.Utilities.DynamicData.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace LightYear.ColdChainDelivery.BL.BE.Account
{
    public class LoginEntity
    {
        /// <summary>
        /// 登入帳號
        /// </summary>
        [DisplayNameCustom("登入帳號")]
        [UIType(UITypeEnum.Input, IsRequired = true, Editable = true)]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Account { get; set; }

        /// <summary>
        /// 密碼
        /// </summary>
        [DisplayNameCustom("密碼")]
        [UIType(UITypeEnum.Input, IsRequired = true, Editable = true)]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Password { get; set; }

        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string ReturnUrl { get; set; }

        public string GoogleToken { get; set; }

        /// <summary>
        /// 廠商代碼，前台登入時使用
        /// </summary>
        public string CompanyCode { get; set; }
    }
}
