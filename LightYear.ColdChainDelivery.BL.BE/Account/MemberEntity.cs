﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.ColdChainDelivery.BL.BE.Account
{
    public class MemberEntity
    {
        public MemberEntity()
        {
            ActiveFlag = true;
            LogisticsCompany = "";
        }

        public long Id { get; set; } // id (Primary key)
        public string Account { get; set; } // account (length: 50)
        public string Password { get; set; } // password (length: 255)
        public string ConfirmPassword { get; set; }
        public string UserName { get; set; } // user_name (length: 100)
        public string UserEmail { get; set; }
        public bool ActiveFlag { get; set; } // active_flag

        /// <summary>
        /// 1:管理者2:客戶3:司機
        /// </summary>
        public string AccountType { get; set; } // account_type (length: 2)
        public string AccountTypeChinese { get; set; }

        /// <summary>
        /// 物流商ID
        /// </summary>
        public int? LogisticsId { get; set; } // logistics_id
        public string LogisticsCompany { get; set; }
        public string CustomerCompanyName { get; set; } // customer_company_name (length: 20)
        public string CustomerCompanyTaxId { get; set; } // customer_company_tax_id (length: 10)
        public string CustomerCompanyAddress { get; set; }
    }

    public class FrontendMemberEntity
    {
        public FrontendMemberEntity()
        {
            ActiveFlag = true;
            LogisticsCompany = "";
            LogisticsId = 0;
        }

        public long Id { get; set; } // id (Primary key)
        public string Account { get; set; } // account (length: 50)
        public string UserName { get; set; } // user_name (length: 100)
        public bool ActiveFlag { get; set; } // active_flag

        /// <summary>
        /// 1:管理者2:客戶3:司機
        /// </summary>
        public string AccountType { get; set; } // account_type (length: 2)
        public string AccountTypeChinese { get; set; }

        /// <summary>
        /// 物流商ID
        /// </summary>
        public int? LogisticsId { get; set; } // logistics_id
        public string LogisticsCompany { get; set; }
    }
}
