﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.ColdChainDelivery.BL.BE.StudentTest;
using LightYear.ColdChainDelivery.DA.ColdChainDeliveryDb;
using LightYear.ColdChainDelivery.DA.ColdChainDeliveryDbReadOnly;
using LightYear.ColdChainDelivery.BL.BE.DeliveryRequest;

namespace LightYear.ColdChainDelivery.Mappers.DeliveryRequest
{
    public class DeliveryRequestMappingProfile : AutoMapper.Profile
    {
        public DeliveryRequestMappingProfile()
        {
            this.CreateMap<CreateDeliveryRequestEntity, DA.ColdChainDeliveryDb.DeliveryRequest>()
                .ForMember(i => i.DeliveryNumber, s => s.MapFrom(i => i.DeliveryNumber))
                .ForMember(i => i.OrderNumber, s => s.MapFrom(i => i.OrderNumber))
                .ForMember(i => i.SenderName, s => s.MapFrom(i => i.SenderName))
                .ForMember(i => i.SenderAddress, s => s.MapFrom(i => i.SenderAddress))
                .ForMember(i => i.SenderZipId, s => s.MapFrom(i => i.SenderCityAreaId))
                .ForMember(i => i.ReceiverName, s => s.MapFrom(i => i.ReceiverName))
                .ForMember(i => i.ReceiverTel1, s => s.MapFrom(i => i.ReceiverTel1))
                .ForMember(i => i.ReceiverTel2, s => s.MapFrom(i => i.ReceiverTel2))
                .ForMember(i => i.ReceiverAddress, s => s.MapFrom(i => i.ReceiverAddress))
                .ForMember(i => i.ReceiverZipId, s => s.MapFrom(i => i.ReceiverCityAreaId))
                .ForMember(i => i.CollectionAmount, s => s.MapFrom(i => i.CollectionAmount));

            this.CreateMap<DA.ColdChainDeliveryDbReadOnly.DeliveryRequest, CreateDeliveryRequestEntity>()
                .ForMember(i => i.DeliveryNumber, s => s.MapFrom(i => i.DeliveryNumber))
                .ForMember(i => i.ProjectNo, s => s.MapFrom(i => i.Project.ProjectNo))
                .ForMember(i => i.OrderNumber, s => s.MapFrom(i => i.OrderNumber))
                .ForMember(i => i.SenderName, s => s.MapFrom(i => i.SenderName))
                .ForMember(i => i.SenderZip, s => s.MapFrom(i => i.SenderZip.Zip))
                .ForMember(i => i.SenderAddress, s => s.MapFrom(i => i.SenderAddress))
                .ForMember(i => i.ReceiverName, s => s.MapFrom(i => i.ReceiverName))
                .ForMember(i => i.ReceiverTel1, s => s.MapFrom(i => i.ReceiverTel1))
                .ForMember(i => i.ReceiverTel2, s => s.MapFrom(i => i.ReceiverTel2))
                .ForMember(i => i.ReceiverZip, s => s.MapFrom(i => i.ReceiverZip.Zip))
                .ForMember(i => i.ReceiverAddress, s => s.MapFrom(i => i.ReceiverAddress))
                .ForMember(i => i.CollectionAmount, s => s.MapFrom(i => i.CollectionAmount))
                .ForMember(i => i.MemberName, s => s.MapFrom(i => i.Project.Member_MemberId.UserName));

            this.CreateMap<DA.ColdChainDeliveryDbReadOnly.Member, MemberEntity>()
                .ForMember(i => i.Id, s => s.MapFrom(i => i.Id))
                .ForMember(i => i.Name, s => s.MapFrom(i => i.UserName));

            this.CreateMap<DA.ColdChainDeliveryDbReadOnly.DeliveryRequest, SimpleDeliveryRequestEntity>()
                .ForMember(i => i.DeliveryNumber, s => s.MapFrom(i => i.DeliveryNumber))
                .ForMember(i => i.OrderNumber, s => s.MapFrom(i => i.OrderNumber))
                .ForMember(i => i.ReceiverName, s => s.MapFrom(i => i.ReceiverName))
                .ForMember(i => i.ReceiverCityAreaId, s => s.MapFrom(i => i.ReceiverZipId))
                .ForMember(i => i.Status, s => s.MapFrom(i => "狀態"));

            this.CreateMap<DA.ColdChainDeliveryDbReadOnly.DeliveryRequest, DeliveryRequestEntity>()
                .ForMember(i => i.Id, s => s.MapFrom(i => i.Id))
                .ForMember(i => i.DeliveryNumber, s => s.MapFrom(i => i.DeliveryNumber))
                .ForMember(i => i.OrderNumber, s => s.MapFrom(i => i.OrderNumber))
                .ForMember(i => i.ProjectNo, s => s.MapFrom(i => i.Project.ProjectNo))
                .ForMember(i => i.ReceiverName, s => s.MapFrom(i => i.ReceiverName))
                .ForMember(i => i.ReceiverCityAreaId, s => s.MapFrom(i => i.ReceiverZipId))
                .ForMember(i => i.ReceiverAddress, s => s.MapFrom(i => i.ReceiverAddress))
                .ForMember(i => i.ReceiverFullAddress, s => s.MapFrom(i => i.ReceiverZip.City + i.ReceiverZip.Area + i.ReceiverAddress))
                .ForMember(i => i.SenderName, s => s.MapFrom(i => i.SenderName))
                .ForMember(i => i.SenderCityAreaId, s => s.MapFrom(i => i.SenderZipId))
                .ForMember(i => i.SenderAddress, s => s.MapFrom(i => i.SenderAddress))
                .ForMember(i => i.SenderFullAddress, s => s.MapFrom(i => i.SenderZip.City + i.SenderZip.Area + i.SenderAddress))
                .ForMember(i => i.CollectionAmount, s => s.MapFrom(i => i.CollectionAmount))
                .ForMember(i => i.Status, s => s.MapFrom(i => "狀態"));

            this.CreateMap<DA.ColdChainDeliveryDb.DeliveryRequest, DeliveryRequestEntity>()
                .ForMember(i => i.Id, s => s.MapFrom(i => i.Id))
                .ForMember(i => i.DeliveryNumber, s => s.MapFrom(i => i.DeliveryNumber))
                .ForMember(i => i.OrderNumber, s => s.MapFrom(i => i.OrderNumber))
                .ForMember(i => i.ProjectNo, s => s.MapFrom(i => i.Project.ProjectNo))
                .ForMember(i => i.ReceiverName, s => s.MapFrom(i => i.ReceiverName))
                .ForMember(i => i.ReceiverCityAreaId, s => s.MapFrom(i => i.ReceiverZipId))
                .ForMember(i => i.ReceiverAddress, s => s.MapFrom(i => i.ReceiverAddress))
                .ForMember(i => i.ReceiverFullAddress, s => s.MapFrom(i => i.ReceiverZip.City + i.ReceiverZip.Area + i.ReceiverAddress))
                .ForMember(i => i.SenderName, s => s.MapFrom(i => i.SenderName))
                .ForMember(i => i.SenderCityAreaId, s => s.MapFrom(i => i.SenderZipId))
                .ForMember(i => i.SenderAddress, s => s.MapFrom(i => i.SenderAddress))
                .ForMember(i => i.SenderFullAddress, s => s.MapFrom(i => i.SenderZip.City + i.SenderZip.Area + i.SenderAddress))
                .ForMember(i => i.CollectionAmount, s => s.MapFrom(i => i.CollectionAmount))
                .ForMember(i => i.Status, s => s.MapFrom(i => "狀態"));

            this.CreateMap<DeliveryRequestEntity, DA.ColdChainDeliveryDb.DeliveryRequest>()
                .ForMember(i => i.Id, s => s.MapFrom(i => i.Id))
                .ForMember(i => i.ReceiverName, s => s.MapFrom(i => i.ReceiverName))
                .ForMember(i => i.ReceiverZipId, s => s.MapFrom(i => i.ReceiverCityAreaId))
                .ForMember(i => i.ReceiverAddress, s => s.MapFrom(i => i.ReceiverAddress))
                .ForMember(i => i.SenderName, s => s.MapFrom(i => i.SenderName))
                .ForMember(i => i.SenderZipId, s => s.MapFrom(i => i.SenderCityAreaId))
                .ForMember(i => i.SenderAddress, s => s.MapFrom(i => i.SenderAddress))
                .ForMember(i => i.CollectionAmount, s => s.MapFrom(i => i.CollectionAmount));

            this.CreateMap<BatchDeliveryRequest, BatchDeliveryRequestForOutputEntity>()
                .ForMember(i => i.Row, s => s.MapFrom(i => i.Row))
                .ForMember(i => i.DeliveryRequestEntity, s => s.MapFrom(i => i.DeliveryRequest))
                .ForMember(i => i.ErrMsg, s => s.MapFrom(i => i.ErrMsg))
                .ForMember(i => i.IsValidate, s => s.MapFrom(i => i.IsValidate));
        }
    }
}