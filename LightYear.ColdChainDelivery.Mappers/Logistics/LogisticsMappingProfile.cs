﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.ColdChainDelivery.Mappers.Logistics
{
    public class LogisticsMappingProfile :AutoMapper.Profile
    {
        public LogisticsMappingProfile()
        {
            CreateMap<DA.ColdChainDeliveryDbReadOnly.Logistic, DA.ColdChainDeliveryDb.Logistic>()
                .ForMember(i => i.Id, s => s.MapFrom(i => i.Id))
                .ForMember(i => i.CreateTime, s => s.MapFrom(i => i.CreateTime))
                .ForMember(i => i.Company, s => s.MapFrom(i => i.Company))
                .ForMember(i => i.CompanyCode, s => s.MapFrom(i => i.CompanyCode))
                .ForMember(i => i.Logo, s => s.MapFrom(i => i.Logo))
                .ForMember(i => i.Active, s => s.MapFrom(i => i.Active));
        }
    }
}
