﻿using LightYear.ColdChainDelivery.BL.BE.Account;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.ColdChainDelivery.Mappers.Account
{
    public class AccountMappingProfile : AutoMapper.Profile
    {
        public AccountMappingProfile()
        {
            CreateMap<DA.ColdChainDeliveryDbReadOnly.Member, UserInfoEntity>()
                    .ForMember(i => i.Id, s => s.MapFrom(i => i.Id))
                    .ForMember(i => i.UserName, s => s.MapFrom(i => i.UserName))
                    .ForMember(i => i.Account, s => s.MapFrom(i => i.Account))
                    .ForMember(i => i.AccountType, s => s.MapFrom(i => i.AccountType))
                    .ForMember(i => i.LogisticId, s => s.MapFrom(i => i.LogisticsId));

            CreateMap<MemberEntity, DA.ColdChainDeliveryDb.Member>()
                   .ForMember(i => i.Id, s => s.MapFrom(i => i.Id))
                   .ForMember(i => i.UserName, s => s.MapFrom(i => i.UserName))
                   .ForMember(i => i.Account, s => s.MapFrom(i => i.Account))
                   .ForMember(i => i.AccountType, s => s.MapFrom(i => i.AccountType))
                   .ForMember(i => i.ActiveFlag, s => s.MapFrom(i => i.ActiveFlag))
                   .ForMember(i => i.CustomerCompanyAddress, s => s.MapFrom(i => i.CustomerCompanyAddress))
                   .ForMember(i => i.CustomerCompanyName, s => s.MapFrom(i => i.CustomerCompanyName))
                   .ForMember(i => i.CustomerCompanyTaxId, s => s.MapFrom(i => i.CustomerCompanyTaxId))
                   .ForMember(i => i.Password, s => s.MapFrom(i => i.Password))
                   .ForMember(i => i.UserEmail, s => s.MapFrom(i => i.UserEmail))
                   .ForMember(i => i.LogisticsId, s => s.MapFrom(i => i.LogisticsId));

            //CreateMap<DA.ColdChainDeliveryDb.Member, MemberEntity>()
            //    .ForMember(i => i.Id, s => s.MapFrom(i => i.Id))
            //    .ForMember(i => i.UserName, s => s.MapFrom(i => i.UserName))
            //    .ForMember(i => i.Account, s => s.MapFrom(i => i.Account))
            //    .ForMember(i => i.AccountType, s => s.MapFrom(i => i.AccountType))
            //    .ForMember(i => i.ActiveFlag, s => s.MapFrom(i => i.ActiveFlag))
            //    .ForMember(i => i.CustomerCompanyAddress, s => s.MapFrom(i => i.CustomerCompanyAddress))
            //    .ForMember(i => i.CustomerCompanyName, s => s.MapFrom(i => i.CustomerCompanyName))
            //    .ForMember(i => i.CustomerCompanyTaxId, s => s.MapFrom(i => i.CustomerCompanyTaxId))
            //    .ForMember(i => i.LogisticsId, s => s.MapFrom(i => i.LogisticsId));

            CreateMap<DA.ColdChainDeliveryDb.Member, FrontendMemberEntity>()
               .ForMember(i => i.Id, s => s.MapFrom(i => i.Id))
               .ForMember(i => i.UserName, s => s.MapFrom(i => i.UserName))
               .ForMember(i => i.Account, s => s.MapFrom(i => i.Account))
               .ForMember(i => i.AccountType, s => s.MapFrom(i => i.AccountType))
               .ForMember(i => i.ActiveFlag, s => s.MapFrom(i => i.ActiveFlag))
               .ForMember(i => i.LogisticsId, s => s.MapFrom(i => i.LogisticsId));

            CreateMap<FrontendMemberEntity, DA.ColdChainDeliveryDb.Member> ()
               .ForMember(i => i.Id, s => s.MapFrom(i => i.Id))
               .ForMember(i => i.UserName, s => s.MapFrom(i => i.UserName))
               .ForMember(i => i.Account, s => s.MapFrom(i => i.Account))
               .ForMember(i => i.AccountType, s => s.MapFrom(i => i.AccountType))
               .ForMember(i => i.ActiveFlag, s => s.MapFrom(i => i.ActiveFlag))
               .ForMember(i => i.LogisticsId, s => s.MapFrom(i => i.LogisticsId));
        }
    }
}
