﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.ColdChainDelivery.BL.BE.StudentTest;
using LightYear.ColdChainDelivery.DA.ColdChainDeliveryDbReadOnly;
using LightYear.ColdChainDelivery.BL.BE.DeliveryRequest;

namespace LightYear.ColdChainDelivery.Mappers.PostCityArea
{
    public class PostCityAreaMappingProfile : AutoMapper.Profile
    {
        public PostCityAreaMappingProfile()
        {
            this.CreateMap<DA.ColdChainDeliveryDbReadOnly.PostCityArea, AreaEntity>()
                .ForMember(i => i.Id, s => s.MapFrom(i => i.Id))
                .ForMember(i => i.Area, s => s.MapFrom(i => i.Area));
        }
    }
}