﻿using LightYear.ColdChainDelivery.BL.Services.StudentTest;
using Autofac;
using System.Reflection;

namespace LightYear.ColdChainDelivery.Modules.BL
{
    /// <summary>
    /// ServiceModule
    /// </summary>
    public class ServiceModule : Autofac.Module
    {
        /// <summary>
        /// Load
        /// </summary>
        /// <param name="builder">ContainerBuilder</param>
        protected override void Load(ContainerBuilder builder)
        {
            //// Register Services
            var service = Assembly.Load("LightYear.ColdChainDelivery.BL.Services");

            builder.RegisterAssemblyTypes(service)
                   .AsImplementedInterfaces();

        }
    }
}
