﻿using System;
using System.Collections.Generic;
using System.Text;
using Autofac;
using LightYear.ColdChainDelivery.DA.ColdChainDeliveryDb;

namespace LightYear.ColdChainDelivery.Modules.DA
{
    public class ColdChainDeliveryDbModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            //base.Load(builder);

            var connectionStringKey = "ColdChainDeliveryDbContext";

            builder.RegisterType<ColdChainDeliveryDbContext>()
                .AsSelf()
                .WithParameter("connectionString", connectionStringKey)
                .InstancePerLifetimeScope();
        }

    }
}
