﻿using System;
using System.Reflection;
using Autofac;

namespace LightYear.ColdChainDelivery.Modules.DA
{
    public class RepositoriesModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            //base.Load(builder);
            var repository = Assembly.Load("LightYear.ColdChainDelivery.DA.Repositories");

            builder.RegisterAssemblyTypes(repository)
                   .AsImplementedInterfaces();
        }

    }
}
