﻿using System;
using System.Collections.Generic;
using System.Text;
using Autofac;
using LightYear.ColdChainDelivery.DA.ColdChainDeliveryDbReadOnly;

namespace LightYear.ColdChainDelivery.Modules.DA
{
    public class ColdChainDeliveryDbReadOnlyModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            //base.Load(builder);

            var connectionStringKey = "ColdChainDeliveryReadOnlyDbContext";

            builder.RegisterType<ColdChainDeliveryReadOnlyDbContext>()
                .AsSelf()
                .WithParameter("connectionString", connectionStringKey)
                .InstancePerLifetimeScope();
        }

    }
}
