CREATE OR REPLACE FUNCTION public.addordernumber()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
DECLARE
		v_delivery_type character varying(20);
		v_year bigint;
		v_month bigint;
		v_order_number character varying(20);
		v_tail character varying(5);
		v_getid bigint;
		v_temp_md integer;
	BEGIN
		if New.delivery_type = 1 THEN
			v_delivery_type := 'B';
		elsif New.delivery_type = 2 THEN
			v_delivery_type := 'H';
		else
			v_delivery_type := 'P';
		end if;
		v_year := EXTRACT(YEAR FROM now()) - 2000;
		v_month := EXTRACT(MONTH FROM now());
		v_getid := New.id + (v_year * 10000000000) + (v_month * 100000000);
		v_temp_md := v_getid % 8;
		v_tail := cast(v_temp_md as varchar);
		v_order_number := concat( v_delivery_type, cast(v_getid as varchar), v_tail);
		update delivery_request set order_number = v_order_number where id = New.id;
		return new;
	END;
$BODY$;

ALTER FUNCTION public.addordernumber()
    OWNER TO postgres;