﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.ColdChainDelivery.DA.ColdChainDeliveryDbReadOnly;
using System.Linq;

namespace LightYear.ColdChainDelivery.DA.Repositories.Member
{
    public class MemberRepository : IMemberRepository
    {
        public ColdChainDeliveryReadOnlyDbContext ColdChainDeliveryReadOnlyDbContext;

        public MemberRepository(ColdChainDeliveryReadOnlyDbContext ColdChainDeliveryReadOnlyDbContext)
        {
            this.ColdChainDeliveryReadOnlyDbContext = ColdChainDeliveryReadOnlyDbContext;
        }

        public ColdChainDeliveryDbReadOnly.Member GetByAccount(string account)
        {
            var data = from member in this.ColdChainDeliveryReadOnlyDbContext.Members where member.Account.Equals(account) && member.IsDeleted == 0 select member;

            if (data.Count() == 0)
            {
                return null;
            }

            return data.FirstOrDefault();
        }

        public ColdChainDeliveryDbReadOnly.Member GetMember(long id)
        {
            ColdChainDeliveryDbReadOnly.Member member = this.ColdChainDeliveryReadOnlyDbContext.Members.Find(id);

            return member;
        }

        public List<ColdChainDeliveryDbReadOnly.Member> GetMembers()
        {
            var data = (from member in this.ColdChainDeliveryReadOnlyDbContext.Members where member.IsDeleted == 0 && member.AccountType == "2" select member).OrderBy(e => e.Id);

            if (data.Count() == 0)
            {
                return null;
            }

            return data.ToList();
        }

        public ColdChainDeliveryDbReadOnly.Member GetMemberByAccountAndHashedPassword(string account, string hashPassword, string companyCode)
        {
            if (string.IsNullOrWhiteSpace(companyCode))
            {
                return ColdChainDeliveryReadOnlyDbContext.Members.Where(m => m.Account == account && m.Password == hashPassword && m.ActiveFlag == 1).FirstOrDefault();
            }
            else
            {
                return (from member in ColdChainDeliveryReadOnlyDbContext.Members join logistic in ColdChainDeliveryReadOnlyDbContext.Logistics on member.LogisticsId equals logistic.Id
                 where member.Account == account && member.Password == hashPassword && member.ActiveFlag == 1 && logistic.CompanyCode == companyCode select member).FirstOrDefault();
            }
        }

        public List<ColdChainDeliveryDbReadOnly.Member> GetMembers(int skip, int pageSize)
        {
            var data = (from member in this.ColdChainDeliveryReadOnlyDbContext.Members where member.IsDeleted == 0 select member).OrderBy(e => e.Id).Skip(skip).Take(pageSize);

            if (data.Count() == 0)
            {
                return null;
            }

            return data.ToList();
        }

        public List<ColdChainDeliveryDbReadOnly.Member> GetMembersOrderByAccount()
        {
            var data = (from member in this.ColdChainDeliveryReadOnlyDbContext.Members where member.IsDeleted == 0 select member).OrderBy(e => e.Account);

            if (data.Count() == 0)
            {
                return null;
            }

            return data.ToList();
        }

        public List<ColdChainDeliveryDbReadOnly.Member> GetMembersOrderByAccount(int skip, int pageSize)
        {
            var data = (from member in this.ColdChainDeliveryReadOnlyDbContext.Members where member.IsDeleted == 0 select member).OrderBy(e => e.Account).Skip(skip).Take(pageSize);

            if (data.Count() == 0)
            {
                return null;
            }

            return data.ToList();
        }

        public ColdChainDeliveryDbReadOnly.Member GetMemberInfo(long id)
        {
            ColdChainDeliveryDbReadOnly.Member member = ColdChainDeliveryReadOnlyDbContext.Members.Where(m => m.Id == id && m.IsDeleted == 0 && m.ActiveFlag == 1).FirstOrDefault();
            return member;
        }

        public List<ColdChainDeliveryDbReadOnly.Member> GetCustomerMembers()
        {
            var data = (from member in this.ColdChainDeliveryReadOnlyDbContext.Members where member.IsDeleted == 0 && member.AccountType == "2" select member).OrderBy(e => e.Id);

            if (data.Count() == 0)
            {
                return null;
            }

            return data.ToList();
        }
    }
}
