﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.ColdChainDelivery.DA.ColdChainDeliveryDb;

namespace LightYear.ColdChainDelivery.DA.Repositories.Member
{
    /// <summary>
    /// 會員資料變更的da
    /// </summary>
    public interface IMemberModifyRepository
    {
        /// <summary>
        /// 新增會員資料
        /// </summary>
        /// <param name="member"></param>
        /// <returns></returns>
        long Insert(ColdChainDeliveryDb.Member member);

        /// <summary>
        /// 變更
        /// </summary>
        /// <param name="member">會員資料變更</param>
        void Update(ColdChainDeliveryDb.Member member);

        /// <summary>
        /// 刪除(假刪除)
        /// </summary>
        /// <param name="id">序號</param>
        void Delete(long id);

        List<ColdChainDeliveryDb.Member> GetMembers();
    }
}
