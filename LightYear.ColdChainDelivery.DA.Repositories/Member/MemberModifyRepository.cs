﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.ColdChainDelivery.DA.ColdChainDeliveryDb;
using System.Linq;

namespace LightYear.ColdChainDelivery.DA.Repositories.Member
{
    public class MemberModifyRepository : IMemberModifyRepository
    {
        public ColdChainDeliveryDbContext ColdChainDeliveryDbContext { get; private set; }

        public MemberModifyRepository(ColdChainDeliveryDbContext ColdChainDeliveryDbContext)
        {
            this.ColdChainDeliveryDbContext = ColdChainDeliveryDbContext;
        }

        public void Delete(long id)
        {
            ColdChainDeliveryDb.Member member = this.ColdChainDeliveryDbContext.Members.Find(id);

            if( member != null  )
            {

                ColdChainDeliveryDbContext.Members.Attach(member);

                var entry = this.ColdChainDeliveryDbContext.Entry(member);

                entry.State = Microsoft.EntityFrameworkCore.EntityState.Modified;

                member.DeletedAt = DateTime.Now;

                member.IsDeleted = 1;

                entry.Property(x => x.CreatedAt).IsModified = false;
                entry.Property(x => x.UpdatedAt).IsModified = false;

                this.ColdChainDeliveryDbContext.SaveChanges();
            }

        }

        public long Insert(ColdChainDeliveryDb.Member member)
        {
            member.CreatedAt = DateTime.Now;
            member.UpdatedAt = DateTime.Now;

            this.ColdChainDeliveryDbContext.Members.Attach(member);

            this.ColdChainDeliveryDbContext.SaveChanges();

            return member.Id;
        }

        public void Update(ColdChainDeliveryDb.Member member)
        {
            var original = GetMember(member.Id);

            original.ActiveFlag = member.ActiveFlag;
            original.AccountType = member.AccountType;
            original.Account = member.Account;
            original.UserName = member.UserName;
            original.LogisticsId = member.LogisticsId;

            ColdChainDeliveryDbContext.Members.Attach(original);

            var entry = this.ColdChainDeliveryDbContext.Entry(original);

            entry.State = Microsoft.EntityFrameworkCore.EntityState.Modified;

            member.UpdatedAt = DateTime.Now;

            entry.Property(x => x.CreatedAt).IsModified = false;
            entry.Property(x => x.DeletedAt).IsModified = false;

            this.ColdChainDeliveryDbContext.SaveChanges();
        }

        public List<ColdChainDeliveryDb.Member> GetMembers()
        {
            var data = (from member in this.ColdChainDeliveryDbContext.Members where member.IsDeleted == 0 select member).OrderBy(e => e.Id);

            if (data.Count() == 0)
            {
                return null;
            }

            return data.ToList();
        }

        public ColdChainDeliveryDb.Member GetMember(long id)
        {
            ColdChainDeliveryDb.Member member = this.ColdChainDeliveryDbContext.Members.Find(id);

            return member;
        }
    }
}
