﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.ColdChainDelivery.DA.ColdChainDeliveryDbReadOnly;

namespace LightYear.ColdChainDelivery.DA.Repositories.Member
{
    public interface IMemberRepository
    {

        ColdChainDeliveryDbReadOnly.Member GetMember(long id);

        ColdChainDeliveryDbReadOnly.Member GetByAccount(string account);

        List<ColdChainDeliveryDbReadOnly.Member> GetMembers();

        List<ColdChainDeliveryDbReadOnly.Member> GetMembers(int skip, int pageSize);

        List<ColdChainDeliveryDbReadOnly.Member> GetMembersOrderByAccount();

        List<ColdChainDeliveryDbReadOnly.Member> GetMembersOrderByAccount(int skip, int pageSize);

        ColdChainDeliveryDbReadOnly.Member GetMemberByAccountAndHashedPassword(string account, string hashPassword, string companyCode);

        ColdChainDeliveryDbReadOnly.Member GetMemberInfo(long id);

        List<ColdChainDeliveryDbReadOnly.Member> GetCustomerMembers();
    }
}
