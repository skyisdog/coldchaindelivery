﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.ColdChainDelivery.BL.BE.DeliveryRequest;

namespace LightYear.ColdChainDelivery.DA.Repositories.DeliveryRequestModify
{
    public interface IDeliveryRequestModifyRepository
    {
        ColdChainDeliveryDb.DeliveryRequest Insert(ColdChainDeliveryDb.DeliveryRequest deliveryRequest);
        long SoftDelete(long id, long updateMemberId);
        DA.ColdChainDeliveryDb.DeliveryRequest PartialEdit(ColdChainDeliveryDb.DeliveryRequest request);
        DA.ColdChainDeliveryDb.DeliveryRequest PartialEditWithSenderInfo(ColdChainDeliveryDb.DeliveryRequest request);
        BatchDeliveryRequest[] BatchInsert(BatchDeliveryRequest[] requests);
    }
}