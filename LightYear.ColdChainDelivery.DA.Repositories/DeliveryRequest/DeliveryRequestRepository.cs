﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.ColdChainDelivery.DA.ColdChainDeliveryDbReadOnly;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace LightYear.ColdChainDelivery.DA.Repositories.DeliveryRequest
{
    public class DeliveryRequestRepository : IDeliveryRequestRepository
    {
        ColdChainDeliveryReadOnlyDbContext ColdChainDeliveryReadOnlyDbContext;
        public DeliveryRequestRepository(ColdChainDeliveryReadOnlyDbContext coldChainDeliveryReadOnlyDbContext) {
            ColdChainDeliveryReadOnlyDbContext = coldChainDeliveryReadOnlyDbContext;
        }

        public IEnumerable<PostCityArea> GetCityArea() {
            return ColdChainDeliveryReadOnlyDbContext.PostCityAreas;
        }

        public ColdChainDeliveryDbReadOnly.DeliveryRequest GetDeliveryRequestById(long id)
        {
            ColdChainDeliveryDbReadOnly.DeliveryRequest request = ColdChainDeliveryReadOnlyDbContext.DeliveryRequests.Find(id);

            ColdChainDeliveryDbReadOnly.Project project = ColdChainDeliveryReadOnlyDbContext.Projects.Find(request.ProjectId);
            request.Project = project;

            ColdChainDeliveryDbReadOnly.PostCityArea senderCityArea = ColdChainDeliveryReadOnlyDbContext.PostCityAreas.Find(request.SenderZipId);
            request.SenderZip = senderCityArea;

            ColdChainDeliveryDbReadOnly.PostCityArea receiverCityArea = ColdChainDeliveryReadOnlyDbContext.PostCityAreas.Find(request.ReceiverZipId);
            request.ReceiverZip = receiverCityArea;

            ColdChainDeliveryDbReadOnly.Member member = ColdChainDeliveryReadOnlyDbContext.Members.Find(project.MemberId);
            request.Project.Member_MemberId = member;

            return request;
        }

        public ColdChainDeliveryDbReadOnly.DeliveryRequest[] GetSimpleDeliveryRequestsByDeliveryNumbers(string[] deliveryNumbers, int logisticId)
        {
            ColdChainDeliveryDbReadOnly.DeliveryRequest[] requests = ColdChainDeliveryReadOnlyDbContext.DeliveryRequests.Include(t=>t.Project).Where(d=>deliveryNumbers.Contains(d.DeliveryNumber) && d.Project.LogisticId == logisticId).ToArray();
            return requests;
        }

        public string GetLogoUrlByLogisticsCode(string companyCode)
        {
            string url = (from logistic in ColdChainDeliveryReadOnlyDbContext.Logistics where logistic.CompanyCode == companyCode
                       select logistic.Logo).FirstOrDefault() ?? "";

            return url;
        }

        public IEnumerable<ColdChainDeliveryDbReadOnly.DeliveryRequest> GetDeliveryRequests(int logisticId)
        {
            IEnumerable<ColdChainDeliveryDbReadOnly.DeliveryRequest> data = ColdChainDeliveryReadOnlyDbContext.DeliveryRequests.Include(x => x.Project).Include(x => x.ReceiverZip).Where(d=>d.DeleteAt == null && d.Project.LogisticId == logisticId).OrderByDescending(o=>o.Id);
            return data;
        }
    }
}