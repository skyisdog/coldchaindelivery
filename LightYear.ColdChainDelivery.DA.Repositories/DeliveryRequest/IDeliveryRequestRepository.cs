﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.ColdChainDelivery.DA.ColdChainDeliveryDbReadOnly;

namespace LightYear.ColdChainDelivery.DA.Repositories.DeliveryRequest
{
    public interface IDeliveryRequestRepository
    {
        IEnumerable<PostCityArea> GetCityArea();
        ColdChainDeliveryDbReadOnly.DeliveryRequest GetDeliveryRequestById(long id);
        ColdChainDeliveryDbReadOnly.DeliveryRequest[] GetSimpleDeliveryRequestsByDeliveryNumbers(string[] deliveryNumbers, int logisticId);
        string GetLogoUrlByLogisticsCode(string companyCode);
        IEnumerable<ColdChainDeliveryDbReadOnly.DeliveryRequest> GetDeliveryRequests(int logisticId);
    }
}