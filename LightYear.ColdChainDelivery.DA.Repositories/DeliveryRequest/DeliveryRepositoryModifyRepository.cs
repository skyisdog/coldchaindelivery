﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.ColdChainDelivery.DA.ColdChainDeliveryDb;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using LightYear.ColdChainDelivery.BL.BE.DeliveryRequest;

namespace LightYear.ColdChainDelivery.DA.Repositories.DeliveryRequestModify
{
    public class DeliveryRequestModify : IDeliveryRequestModifyRepository
    {
        ColdChainDeliveryDbContext ColdChainDeliveryModifyDbContext;
        public DeliveryRequestModify(ColdChainDeliveryDbContext coldChainDeliveryModifyDbContext)
        {
            ColdChainDeliveryModifyDbContext = coldChainDeliveryModifyDbContext;
        }

        public ColdChainDeliveryDb.DeliveryRequest Insert(ColdChainDeliveryDb.DeliveryRequest deliveryRequest)
        {
            ColdChainDeliveryModifyDbContext.DeliveryRequests.Add(deliveryRequest);
            ColdChainDeliveryModifyDbContext.SaveChanges();
            return deliveryRequest;
        }

        public long SoftDelete(long id, long updateMemberId)
        {
            ColdChainDeliveryDb.DeliveryRequest deliveryRequest = ColdChainDeliveryModifyDbContext.DeliveryRequests.Find(id);
            deliveryRequest.DeleteAt = DateTime.Now;
            deliveryRequest.UpdateMemberId = updateMemberId;
            ColdChainDeliveryModifyDbContext.DeliveryRequests.Append(deliveryRequest);
            ColdChainDeliveryModifyDbContext.SaveChanges();
            return id;
        }

        public DA.ColdChainDeliveryDb.DeliveryRequest PartialEdit(ColdChainDeliveryDb.DeliveryRequest request)
        {
            ColdChainDeliveryDb.DeliveryRequest deliveryRequest = ColdChainDeliveryModifyDbContext.DeliveryRequests.Include(x => x.Project).Include(x => x.ReceiverZip).Include(x => x.SenderZip).Where(d => d.Id == request.Id).FirstOrDefault();
            deliveryRequest.ReceiverName = request.ReceiverName;
            deliveryRequest.CollectionAmount = request.CollectionAmount;
            deliveryRequest.ReceiverZipId = request.ReceiverZipId;
            deliveryRequest.ReceiverAddress = request.ReceiverAddress;
            ColdChainDeliveryModifyDbContext.DeliveryRequests.Append(deliveryRequest);
            ColdChainDeliveryModifyDbContext.SaveChanges();
            return deliveryRequest;
        }

        public DA.ColdChainDeliveryDb.DeliveryRequest PartialEditWithSenderInfo(ColdChainDeliveryDb.DeliveryRequest request)
        {
            ColdChainDeliveryDb.DeliveryRequest deliveryRequest = ColdChainDeliveryModifyDbContext.DeliveryRequests.Include(x => x.Project).Include(x => x.ReceiverZip).Include(x => x.SenderZip).Where(d => d.Id == request.Id).FirstOrDefault();
            deliveryRequest.ReceiverName = request.ReceiverName;
            deliveryRequest.CollectionAmount = request.CollectionAmount;
            deliveryRequest.ReceiverZipId = request.ReceiverZipId;
            deliveryRequest.ReceiverAddress = request.ReceiverAddress;

            deliveryRequest.SenderName = request.SenderName;
            deliveryRequest.SenderZipId = request.SenderZipId;
            deliveryRequest.SenderAddress = request.SenderAddress;
            ColdChainDeliveryModifyDbContext.DeliveryRequests.Append(deliveryRequest);
            ColdChainDeliveryModifyDbContext.SaveChanges();
            return deliveryRequest;
        }

        public BatchDeliveryRequest[] BatchInsert(BatchDeliveryRequest[] requests)
        {
            for (int r = 0; r < requests.Length; r++)
            {
                try
                {
                    ColdChainDeliveryModifyDbContext.DeliveryRequests.Add(requests[r].DeliveryRequest);
                    ColdChainDeliveryModifyDbContext.SaveChanges();
                }
                catch
                {
                    requests[r].ErrMsg = "資料錯誤";
                }
            }
            return requests;
        }
    }
}