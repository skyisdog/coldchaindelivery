﻿using LightYear.ColdChainDelivery.DA.ColdChainDeliveryDb;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.ColdChainDelivery.DA.Repositories.Logistics
{
    public interface ILogisticsModifyRepository
    {
        int Insert(Logistic logistic);

        void Update(Logistic logistic);

        void FakeDelete(Logistic logistic);

        DA.ColdChainDeliveryDb.Logistic GetLogisticById(int id);
    }
}
