﻿using LightYear.ColdChainDelivery.DA.ColdChainDeliveryDbReadOnly;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LightYear.ColdChainDelivery.DA.Repositories.Logistics
{
    public class LogisticsRepository: ILogisticsRepository
    {
        public ColdChainDeliveryReadOnlyDbContext ColdChainDeliveryReadOnlyDbContext { get; private set; }

        public LogisticsRepository(ColdChainDeliveryReadOnlyDbContext coldChainDeliveryReadOnlyDbContext)
        {
            ColdChainDeliveryReadOnlyDbContext = coldChainDeliveryReadOnlyDbContext;
        }

        public List<DA.ColdChainDeliveryDbReadOnly.Logistic> GetLogistics()
        {
            return ColdChainDeliveryReadOnlyDbContext.Logistics.ToList();
        }

        public DA.ColdChainDeliveryDbReadOnly.Logistic GetLogisticById(int id)
        {
            return ColdChainDeliveryReadOnlyDbContext.Logistics.Where(a => a.Id == id).FirstOrDefault();
        }

        public int? GetLogisticIdByCompanyCode(string companyCode)
        {
            return ColdChainDeliveryReadOnlyDbContext.Logistics.Where(l => l.CompanyCode == companyCode).FirstOrDefault()?.Id;
        }
    }
}
