﻿using LightYear.ColdChainDelivery.DA.ColdChainDeliveryDbReadOnly;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.ColdChainDelivery.DA.Repositories.Logistics
{
    public interface ILogisticsRepository
    {
        List<Logistic> GetLogistics();
        Logistic GetLogisticById(int id);
        int? GetLogisticIdByCompanyCode(string companyCode);
    }
}
