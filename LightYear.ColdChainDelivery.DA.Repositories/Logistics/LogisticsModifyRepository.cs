﻿using LightYear.ColdChainDelivery.DA.ColdChainDeliveryDb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;

namespace LightYear.ColdChainDelivery.DA.Repositories.Logistics
{
    public class LogisticsModifyRepository: ILogisticsModifyRepository
    {
        public ColdChainDeliveryDbContext ColdChainDeliveryDbContext { get; private set; }
        ILogisticsRepository LogisticsRepository { get; set; }
        IMapper Mapper { get; set; }

        public LogisticsModifyRepository(ColdChainDeliveryDbContext coldChainDeliveryDbContext, ILogisticsRepository logisticsRepository, IMapper mapper)
        {
            ColdChainDeliveryDbContext = coldChainDeliveryDbContext;
            LogisticsRepository = logisticsRepository;
            Mapper = mapper;
        }

        public int Insert(Logistic logistic)
        {
            logistic.CreateTime = DateTime.Now;

            this.ColdChainDeliveryDbContext.Logistics.Add(logistic);

            this.ColdChainDeliveryDbContext.SaveChanges();

            return logistic.Id;
        }

        public void Update(ColdChainDeliveryDb.Logistic item)
        {
            ColdChainDeliveryDbContext.Logistics.Attach(item);

            var entry = this.ColdChainDeliveryDbContext.Entry(item);

            entry.State = Microsoft.EntityFrameworkCore.EntityState.Modified;

            DA.ColdChainDeliveryDbReadOnly.Logistic logistic = LogisticsRepository.GetLogisticById(item.Id);

            if (string.IsNullOrEmpty(item.Logo))
                item.Logo = logistic.Logo;

            entry.Property(x => x.CreateTime).IsModified = false;

            this.ColdChainDeliveryDbContext.SaveChanges();
        }

        public void FakeDelete(Logistic logistic)
        {
            var output = ColdChainDeliveryDbContext.Logistics.Where(a=>a.Id == logistic.Id).FirstOrDefault();

            if (output != null)
                output.Active = 0;

            this.ColdChainDeliveryDbContext.Logistics.Attach(output);

            var entry = this.ColdChainDeliveryDbContext.Entry(output);

            entry.State = Microsoft.EntityFrameworkCore.EntityState.Modified;

            this.ColdChainDeliveryDbContext.SaveChanges();
        }

        public DA.ColdChainDeliveryDb.Logistic GetLogisticById(int id)
        {
            return ColdChainDeliveryDbContext.Logistics.Where(a => a.Id == id).FirstOrDefault();
        }
    }
}
