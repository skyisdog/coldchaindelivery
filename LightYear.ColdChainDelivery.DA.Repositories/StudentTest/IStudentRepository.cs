﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.ColdChainDelivery.DA.ColdChainDeliveryDb;
using LightYear.ColdChainDelivery.DA.ColdChainDeliveryDbReadOnly;
using Student = LightYear.ColdChainDelivery.DA.ColdChainDeliveryDbReadOnly.Student;

namespace LightYear.ColdChainDelivery.DA.Repositories.StudentTest
{
    public interface IStudentRepository
    {
        long Insert(ColdChainDeliveryDb.Student student);

        void Update(ColdChainDeliveryDb.Student student);

        List<Student> GetStudents();

        Student GetStudent(long id);

        void Delete(long id);

    }
}
