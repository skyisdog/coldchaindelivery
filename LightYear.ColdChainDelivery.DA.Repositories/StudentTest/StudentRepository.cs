﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.ColdChainDelivery.DA.ColdChainDeliveryDb;
using LightYear.ColdChainDelivery.DA.ColdChainDeliveryDbReadOnly;
using Student = LightYear.ColdChainDelivery.DA.ColdChainDeliveryDbReadOnly.Student;
using System.Linq;

namespace LightYear.ColdChainDelivery.DA.Repositories.StudentTest
{
    public class StudentRepository : IStudentRepository
    {
        //public LightYearTestDbContext LightYearTestDbContext { get; private set; }
        public ColdChainDeliveryDbContext ColdChainDeliveryDbContext { get; private set; }

        public ColdChainDeliveryReadOnlyDbContext ColdChainDeliveryReadOnlyDbContext { get; private set; }

        public StudentRepository(ColdChainDeliveryDbContext coldChainDeliveryDbContext, ColdChainDeliveryReadOnlyDbContext coldChainDeliveryReadOnlyDbContext)
        {
            this.ColdChainDeliveryReadOnlyDbContext = coldChainDeliveryReadOnlyDbContext;
            this.ColdChainDeliveryDbContext = coldChainDeliveryDbContext;
        }

        public Student GetStudent(long id)
        {
            var data = from student in this.ColdChainDeliveryReadOnlyDbContext.Students where student.StudentId.Equals(id) select student;

            if (data.Count() == 0)
            {
                return null;
            }

            return data.FirstOrDefault();
        }

        public List<Student> GetStudents()
        {
            var data = from student in this.ColdChainDeliveryReadOnlyDbContext.Students orderby student.StudentId descending select student;

            return data.ToList();
        }

        public long Insert(ColdChainDeliveryDb.Student student)
        {
            ColdChainDeliveryDbContext.Students.Add(student);

            ColdChainDeliveryDbContext.SaveChanges();

            return student.StudentId;
        }

        public void Update(ColdChainDeliveryDb.Student student)
        {
            ColdChainDeliveryDbContext.Students.Attach(student);

            var entry = ColdChainDeliveryDbContext.Entry(student);

            entry.State = Microsoft.EntityFrameworkCore.EntityState.Modified;

            ColdChainDeliveryDbContext.SaveChanges();
        }

        public void Delete(long id)
        {
            var student = ColdChainDeliveryDbContext.Students.Find(id);

            if (student != null)
            {
                ColdChainDeliveryDbContext.Students.Remove(student);

                ColdChainDeliveryDbContext.SaveChanges();
            }
        }
    }
}
