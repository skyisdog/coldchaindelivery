﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.ColdChainDelivery.BL.BE.DeliveryRequest;
using LightYear.ColdChainDelivery.DA.ColdChainDeliveryDbReadOnly;
using Microsoft.AspNetCore.Http;

namespace LightYear.ColdChainDelivery.BL.Services.DeliveryRequest
{
    public interface IDeliveryRequestService
    {
        CreateDeliveryRequestEntity GetCreateOrderDafault(long memberId);
        IEnumerable<CityAreaEntity> PostCityAreas();
        long Insert(CreateDeliveryRequestEntity createDeliveryRequestEntity, int logisticId);
        CreateDeliveryRequestEntity GetDeliveryRequestEntity(long id);
        List<MemberEntity> GetCustomerMemberList();
        SimpleDeliveryRequestEntity[] GetSimpleDeliveryRequestByDeliveryNumbers(string[] deliveryNumbers, int logisticId);
        string GetLogoUrlByLogisticsCode(string companyCode);
        DeliveryRequestEntity[] SearchDeliveryRequest(DeliveryRequestInputEntity inputEntity, int logisticId);
        long SoftDelete(long id, long updateMemberId);
        DeliveryRequestEntity PartialEdit(DeliveryRequestEntity deliveryRequest, long updateMemberId);
        DeliveryRequestEntity PartialEditWithSenderInfo(DeliveryRequestEntity deliveryRequest, long updateMemberId);
        BatchDeliveryRequestResultEntity SaveBatchDeliveryRequests(IFormFile file, long memberId, long createMemberId, int logisticId);
        byte[] CreateDefaultBatchCreateDeliveryRequestsFile();
    }
}