﻿using LightYear.ColdChainDelivery.DA.Repositories.Member;
using LightYear.ColdChainDelivery.DA.Repositories.DeliveryRequest;
using LightYear.ColdChainDelivery.BL.BE.DeliveryRequest;
using LightYear.ColdChainDelivery.DA.ColdChainDeliveryDb;
using LightYear.ColdChainDelivery.DA.ColdChainDeliveryDbReadOnly;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using AutoMapper;
using LightYear.ColdChainDelivery.DA.Repositories.DeliveryRequestModify;
using ClosedXML.Excel;
using System.IO;
using Microsoft.AspNetCore.Http;

namespace LightYear.ColdChainDelivery.BL.Services.DeliveryRequest
{
    public class DeliveryRequestService : IDeliveryRequestService
    {
        IDeliveryRequestRepository DeliveryRequestRepository;
        IMemberRepository MemberRepository;
        IMapper Mapper;
        IDeliveryRequestModifyRepository DeliveryRequestModifyRepository;
        public DeliveryRequestService(IMemberRepository memberRepository, IDeliveryRequestRepository deliveryRequestRepository, IMapper mapper, IDeliveryRequestModifyRepository deliveryRequestModifyRepository)
        {
            MemberRepository = memberRepository;
            DeliveryRequestRepository = deliveryRequestRepository;
            Mapper = mapper;
            DeliveryRequestModifyRepository = deliveryRequestModifyRepository;
        }

        BatchExcelCellEntity[] Titles = new BatchExcelCellEntity[] {
            new BatchExcelCellEntity {CellPosition = "A1", Content= "訂單編號" },
            new BatchExcelCellEntity {CellPosition = "B1", Content= "收件人姓名" },
            new BatchExcelCellEntity {CellPosition = "C1", Content= "收件人電話1" },
            new BatchExcelCellEntity {CellPosition = "D1", Content = "收件人電話2" },
            new BatchExcelCellEntity {CellPosition = "E1", Content = "收件地址" },
            new BatchExcelCellEntity {CellPosition = "F1", Content = "代收金額" },
            new BatchExcelCellEntity {CellPosition = "G1", Content = "寄件人姓名" },
            new BatchExcelCellEntity {CellPosition = "H1", Content = "寄件人地址" }};

        public CreateDeliveryRequestEntity GetCreateOrderDafault(long memberId)
        {
            DA.ColdChainDeliveryDbReadOnly.Member member = MemberRepository.GetMemberInfo(memberId);
            //TODO:取得主聯絡人資訊

            CreateDeliveryRequestEntity defaultDeliveryRequestEntity = new CreateDeliveryRequestEntity
            {
                MemberId = member.Id,
                MemberName = member.UserName,
                ProjectNo = CreateProjectNo(),
                DeliveryNumber = "",
                OrderNumber = "",
                ReceiverName = "",
                ReceiverTel1 = "",
                ReceiverTel2 = "",
                ReceiverCityAreaId = 1,
                ReceiverAddress = "",
                CollectionAmount = 0,
                SenderName = "寄件人姓名",        //TODO: Fake Data, wating for MainContact
                SenderCityAreaId = 19,    //TODO: Fake Data, wating for MainContact 
                SenderAddress = "寄件人地址"        //TODO: Fake Data, wating for MainContact
            };

            return defaultDeliveryRequestEntity;
        }

        public IEnumerable<CityAreaEntity> PostCityAreas()
        {
            var cityAreas = DeliveryRequestRepository.GetCityArea();
            var groupedCityArea = cityAreas.GroupBy(ca => ca.City).Select(s => new CityAreaEntity { City = s.Key, AreaEntities = Mapper.Map<AreaEntity[]>(s.ToArray()) }).ToArray();

            for (int i = 0; i < groupedCityArea.Length; i++)
            {
                groupedCityArea[i].CityIndex = i;
            }

            return groupedCityArea;
        }

        public long Insert(CreateDeliveryRequestEntity createDeliveryRequestEntity, int logisticId)
        {
            createDeliveryRequestEntity.ReceiverZip = FromCityAreaIdToZipId(createDeliveryRequestEntity.ReceiverCityAreaId);
            createDeliveryRequestEntity.SenderZip = FromCityAreaIdToZipId(createDeliveryRequestEntity.SenderCityAreaId);

            DA.ColdChainDeliveryDb.DeliveryRequest request = new DA.ColdChainDeliveryDb.DeliveryRequest
            {
                DeliveryNumber = createDeliveryRequestEntity.DeliveryNumber,
                OrderNumber = createDeliveryRequestEntity.OrderNumber,
                SenderName = createDeliveryRequestEntity.SenderName,
                SenderAddress = createDeliveryRequestEntity.SenderAddress,
                SenderZipId = createDeliveryRequestEntity.SenderCityAreaId,
                ReceiverName = createDeliveryRequestEntity.ReceiverName,
                ReceiverTel1 = createDeliveryRequestEntity.ReceiverTel1,
                ReceiverTel2 = createDeliveryRequestEntity.ReceiverTel2,
                ReceiverAddress = createDeliveryRequestEntity.ReceiverAddress,
                ReceiverZipId = createDeliveryRequestEntity.ReceiverCityAreaId,
                CollectionAmount = createDeliveryRequestEntity.CollectionAmount,
                UpdateMemberId = createDeliveryRequestEntity.CreateMemberId,
                CreateMemberId = createDeliveryRequestEntity.CreateMemberId,
            };

            var deliveryRequest = Mapper.Map<DA.ColdChainDeliveryDb.DeliveryRequest>(request);

            deliveryRequest.Project = new DA.ColdChainDeliveryDb.Project
            {
                MemberId = createDeliveryRequestEntity.MemberId,
                CreateDate = DateTime.Now,
                CreateMemberId = createDeliveryRequestEntity.CreateMemberId,
                ProjectNo = createDeliveryRequestEntity.ProjectNo,
                LogisticId = logisticId
            };

            DeliveryRequestModifyRepository.Insert(deliveryRequest);

            return deliveryRequest.Id;
        }

        public CreateDeliveryRequestEntity GetDeliveryRequestEntity(long id)
        {
            DA.ColdChainDeliveryDbReadOnly.DeliveryRequest deliveryRequest = DeliveryRequestRepository.GetDeliveryRequestById(id);
            CreateDeliveryRequestEntity entity = Mapper.Map<CreateDeliveryRequestEntity>(deliveryRequest);

            DA.ColdChainDeliveryDbReadOnly.PostCityArea[] postCityAreas = DeliveryRequestRepository.GetCityArea().ToArray();
            entity.SenderFullAddress = deliveryRequest.SenderZip.City + deliveryRequest.SenderZip.Area + deliveryRequest.SenderAddress;
            entity.ReceiverFullAddress = deliveryRequest.ReceiverZip.City + deliveryRequest.ReceiverZip.Area + deliveryRequest.ReceiverAddress;

            return entity;
        }

        public List<MemberEntity> GetCustomerMemberList()
        {
            List<DA.ColdChainDeliveryDbReadOnly.Member> members = MemberRepository.GetCustomerMembers();
            List<MemberEntity> memberList = Mapper.Map<List<MemberEntity>>(members);

            return memberList;
        }

        public SimpleDeliveryRequestEntity[] GetSimpleDeliveryRequestByDeliveryNumbers(string[] deliveryNumbers, int logisticId)
        {
            DA.ColdChainDeliveryDbReadOnly.DeliveryRequest[] deliveryRequests = DeliveryRequestRepository.GetSimpleDeliveryRequestsByDeliveryNumbers(deliveryNumbers, logisticId);
            SimpleDeliveryRequestEntity[] simpleDeliveryRequestEntities = Mapper.Map<SimpleDeliveryRequestEntity[]>(deliveryRequests);
            DA.ColdChainDeliveryDbReadOnly.PostCityArea[] postCityAreas = DeliveryRequestRepository.GetCityArea().ToArray();
            for (var i = 0; i < simpleDeliveryRequestEntities.Length; i++)
            {
                var cityArea = postCityAreas.Where(a => a.Id == simpleDeliveryRequestEntities[i].ReceiverCityAreaId).FirstOrDefault();
                simpleDeliveryRequestEntities[i].ReceiverCityArea = cityArea.City + cityArea.Area;
                simpleDeliveryRequestEntities[i].ReceiverName = StringMasker(simpleDeliveryRequestEntities[i].ReceiverName);
            }

            string[] notFoundDeliveryNumbers = deliveryNumbers.Except(simpleDeliveryRequestEntities.Select(s => s.DeliveryNumber).ToArray()).ToArray();
            List<SimpleDeliveryRequestEntity> notFoundDeliveryRequests = new List<SimpleDeliveryRequestEntity>();
            for (int n = 0; n < notFoundDeliveryNumbers.Length; n++)
            {
                notFoundDeliveryRequests.Add(new SimpleDeliveryRequestEntity { DeliveryNumber = notFoundDeliveryNumbers[n] + "(查無託運單)" });
            }


            SimpleDeliveryRequestEntity[] resultDeliveryNumbers = simpleDeliveryRequestEntities.Concat(notFoundDeliveryRequests.ToArray()).ToArray();

            return resultDeliveryNumbers;
        }

        public string GetLogoUrlByLogisticsCode(string companyCode)
        {
            return DeliveryRequestRepository.GetLogoUrlByLogisticsCode(companyCode);
        }

        public DeliveryRequestEntity[] SearchDeliveryRequest(DeliveryRequestInputEntity inputEntity, int logisticId)
        {
            IEnumerable<DA.ColdChainDeliveryDbReadOnly.DeliveryRequest> deliveryRequests = DeliveryRequestRepository.GetDeliveryRequests(logisticId);
            if (!String.IsNullOrWhiteSpace(inputEntity.DeliveryNumber))
            {
                deliveryRequests = deliveryRequests.Where(d => d.DeliveryNumber == inputEntity.DeliveryNumber);
            }

            if (!String.IsNullOrWhiteSpace(inputEntity.OrderNumber))
            {
                deliveryRequests = deliveryRequests.Where(d => d.OrderNumber == inputEntity.OrderNumber);
            }

            if (!String.IsNullOrWhiteSpace(inputEntity.ProjectNo))
            {
                deliveryRequests = deliveryRequests.Where(d => d.Project.ProjectNo == inputEntity.ProjectNo);
            }

            if (!String.IsNullOrWhiteSpace(inputEntity.ReceiverName))
            {
                deliveryRequests = deliveryRequests.Where(d => d.ReceiverName == inputEntity.ReceiverName);
            }

            deliveryRequests = deliveryRequests.Where(d => d.Project.CreateDate > inputEntity.CreateDateStart);

            deliveryRequests = deliveryRequests.Where(d => d.Project.CreateDate < inputEntity.CreateDateEnd.AddDays(1));

            //TODO:未寫配達日期篩選邏輯

            DeliveryRequestEntity[] outputEntities = Mapper.Map<DeliveryRequestEntity[]>(deliveryRequests);

            for (var o = 0; o < outputEntities.Length; o++)
            {
                outputEntities[o].ReceiverCityIndex = FromCityAreaIdToCityIndex(outputEntities[o].ReceiverCityAreaId);
            }

            //TODO: 此處寫IsOnRoad(IsAllowEdit)邏輯
            //outputEntities[1].IsAllowEdit = true;

            return outputEntities;
        }

        public long SoftDelete(long id, long updateMemberId)
        {
            DeliveryRequestModifyRepository.SoftDelete(id, updateMemberId);
            return id;
        }

        public DeliveryRequestEntity PartialEdit(DeliveryRequestEntity deliveryRequest, long updateMemberId)
        {
            //TODO: isOnRoad，是的話必須拒絕
            DA.ColdChainDeliveryDb.DeliveryRequest request = Mapper.Map<DA.ColdChainDeliveryDb.DeliveryRequest>(deliveryRequest);
            //加上更新者
            request.UpdateMemberId = updateMemberId;

            DA.ColdChainDeliveryDb.DeliveryRequest editedEntity = DeliveryRequestModifyRepository.PartialEdit(request);
            DeliveryRequestEntity returnRequest = Mapper.Map<DeliveryRequestEntity>(editedEntity);
            returnRequest.ReceiverCityIndex = FromCityAreaIdToCityIndex(returnRequest.ReceiverCityAreaId);
            returnRequest.SenderCityIndex = FromCityAreaIdToCityIndex(returnRequest.SenderCityAreaId);
            //TODO:再判斷一次isOnRoad
            return returnRequest;
        }

        public DeliveryRequestEntity PartialEditWithSenderInfo(DeliveryRequestEntity deliveryRequest, long updateMemberId)
        {
            DA.ColdChainDeliveryDb.DeliveryRequest request = Mapper.Map<DA.ColdChainDeliveryDb.DeliveryRequest>(deliveryRequest);
            //加上更新者
            request.UpdateMemberId = updateMemberId;

            DA.ColdChainDeliveryDb.DeliveryRequest editedEntity = DeliveryRequestModifyRepository.PartialEditWithSenderInfo(request);
            DeliveryRequestEntity returnRequest = Mapper.Map<DeliveryRequestEntity>(editedEntity);
            returnRequest.ReceiverCityIndex = FromCityAreaIdToCityIndex(returnRequest.ReceiverCityAreaId);
            returnRequest.SenderCityIndex = FromCityAreaIdToCityIndex(returnRequest.SenderCityAreaId);
            return returnRequest;
        }

        public BatchDeliveryRequestResultEntity SaveBatchDeliveryRequests(IFormFile file, long memberId, long createMemberId, int logisticId)
        {
            List<BatchDeliveryRequest> batchDeliveryReqeusts = new List<BatchDeliveryRequest>();
            string msg = FormatExcelBatchFile(file, memberId, batchDeliveryReqeusts, createMemberId, logisticId);
            if (!String.IsNullOrWhiteSpace(msg))
            {
                return new BatchDeliveryRequestResultEntity { BatchRequests = Mapper.Map<BatchDeliveryRequestForOutputEntity[]>(batchDeliveryReqeusts), ExcelErrMsg = msg };
            }

            BatchDeliveryRequest[] okRequests = batchDeliveryReqeusts.Where(b => b.IsValidate).ToArray();
            DeliveryRequestModifyRepository.BatchInsert(okRequests);

            BatchDeliveryRequestResultEntity resultEntity = new BatchDeliveryRequestResultEntity { BatchRequests = Mapper.Map<BatchDeliveryRequestForOutputEntity[]>(batchDeliveryReqeusts.ToArray()) };
            for (var r = 0; r < resultEntity.BatchRequests.Length; r++)
            {
                resultEntity.BatchRequests[r].DeliveryRequestEntity.ReceiverCityIndex = FromCityAreaIdToCityIndex(resultEntity.BatchRequests[r].DeliveryRequestEntity.ReceiverCityAreaId);
                resultEntity.BatchRequests[r].DeliveryRequestEntity.SenderCityIndex = FromCityAreaIdToCityIndex(resultEntity.BatchRequests[r].DeliveryRequestEntity.SenderCityAreaId);
            }

            return resultEntity;
        }

        public byte[] CreateDefaultBatchCreateDeliveryRequestsFile()
        {
            var wb = new XLWorkbook();
            wb.Worksheets.Add("託運單");
            var ws = wb.Worksheets.First();
            for (int i = 0; i < Titles.Length; i++)
            {
                ws.Cell(Titles[i].CellPosition).Value = Titles[i].Content;
            }

            //電話
            ws.Column("C").Width = 20;
            ws.Column("D").Width = 20;
            ws.Column("C").SetDataType(XLDataType.Text);
            ws.Column("D").SetDataType(XLDataType.Text);

            //地址
            ws.Column("E").Width = 40;
            ws.Column("H").Width = 40;

            var workbookBytes = new byte[0];
            using (var ms = new MemoryStream())
            {
                wb.SaveAs(ms);
                workbookBytes = ms.ToArray();
            }
            return workbookBytes;
        }

        private string CreateProjectNo()
        {
            DateTime now = DateTime.Now;
            string prefix = now.ToString("yyMMddHHmmssfff");
            long calculator = long.Parse(prefix);

            while (calculator > 9)
            {
                long subSum = 0;
                string calculatorInString = calculator.ToString();
                for (int c = 0; c < calculatorInString.Length; c++)
                {
                    subSum += int.Parse(calculatorInString.Substring(c, 1));
                }

                calculator = subSum;
            }

            return prefix + calculator.ToString();
        }

        private long FromCityAreaIdToZipId(long id)
        {
            CityAreaEntity[] postCityArea = PostCityAreas().ToArray();
            for (int i = 0; i < postCityArea.Length; i++)
            {
                var filter = postCityArea[i].AreaEntities.FirstOrDefault(a => a.Id == id);
                if (filter != null)
                {
                    return filter.Id;
                }
            }

            return -1;
        }

        private long FromCityAreaIdToCityIndex(long id)
        {
            CityAreaEntity[] postCityArea = PostCityAreas().ToArray();
            for (int i = 0; i < postCityArea.Length; i++)
            {
                var filter = postCityArea[i].AreaEntities.FirstOrDefault(a => a.Id == id);
                if (filter != null)
                {
                    return i;
                }
            }

            return -1;
        }

        private string StringMasker(string s)
        {
            if (s == "" || s == null)
            {
                return "";
            }
            else
            {
                if (s.Length > 1)
                {
                    string result = s.Substring(0, 1);
                    for (int i = 0; i < s.Length - 1; i++)
                    {
                        result += "Ｏ";
                    }
                    return result;
                }
                else
                {
                    return s;
                }
            }
        }

        private string FormatExcelBatchFile(IFormFile file, long memberId, List<BatchDeliveryRequest> batchDeliveryRequests, long createMemberId, int logisticId)
        {
            var result = new StringBuilder();
            string resultMessage = "";
            string projectNo = CreateProjectNo();

            using (XLWorkbook wb = new XLWorkbook(file.OpenReadStream()))
            {
                IXLWorksheet ws = wb.Worksheets.First();
                resultMessage = BatchExcelValidation(ws, batchDeliveryRequests, projectNo, memberId, createMemberId, logisticId);
            }

            return resultMessage;
        }

        private string BatchExcelValidation(IXLWorksheet ws, List<BatchDeliveryRequest> deliveryRequests, string projectNo, long memberId, long createMemberId, int logisticId)
        {
            bool isTitleOk = CheckExcelTitle(ws);
            DateTime now = DateTime.Now;
            CityAreaEntity[] cityAreaEntities = PostCityAreas().ToArray();
            DA.ColdChainDeliveryDbReadOnly.Member createMember = MemberRepository.GetMemberInfo(createMemberId);
            if (!isTitleOk)
            {
                return "格式錯誤，請確認是否為最新版本Excel格式";
            }

            int lastestRow = ws.LastRowUsed().RowNumber();
            for (int r = 2; r < lastestRow + 1; r++)
            {
                //檢驗收件人地址
                string receiverFullAddress = ws.Cell("E" + r.ToString()).Value.ToString();
                CityAreaIdAddressEntity receiverSplitAddress = SplitFullAddress(receiverFullAddress, cityAreaEntities);

                //檢驗寄件者地址
                string senderFullAddress = ws.Cell("H" + r.ToString()).Value.ToString();
                CityAreaIdAddressEntity senderSplitAddress = SplitFullAddress(senderFullAddress, cityAreaEntities);

                //檢驗代收貨款
                int collectionAmountInInt;
                string collectionAmount = ws.Cell("F" + r.ToString()).Value.ToString();
                bool isCollectionAmountValidate = int.TryParse(collectionAmount, out collectionAmountInInt) && collectionAmountInInt >= 0;

                DA.ColdChainDeliveryDb.Project project = new DA.ColdChainDeliveryDb.Project
                {
                    MemberId = memberId,
                    CreateDate = now,
                    CreateMemberId = createMemberId,
                    LogisticId = logisticId,
                    ProjectNo = projectNo
                };

                BatchDeliveryRequest request = new BatchDeliveryRequest();
                try
                {
                    request = new BatchDeliveryRequest
                    {
                        Row = r,
                        DeliveryRequest = new DA.ColdChainDeliveryDb.DeliveryRequest
                        {
                            Project = project,
                            OrderNumber = ws.Cell("A" + r.ToString()).Value.ToString(),
                            SenderName = ws.Cell("G" + r.ToString()).Value.ToString(),
                            SenderZipId = senderSplitAddress.CityAreaId,
                            SenderAddress = senderSplitAddress.Address,
                            ReceiverName = ws.Cell("B" + r.ToString()).Value.ToString(),
                            ReceiverTel1 = ws.Cell("C" + r.ToString()).Value.ToString(),
                            ReceiverTel2 = ws.Cell("D" + r.ToString()).Value.ToString(),
                            ReceiverZipId = receiverSplitAddress.CityAreaId,
                            ReceiverAddress = receiverSplitAddress.Address,
                            CollectionAmount = isCollectionAmountValidate ? collectionAmountInInt : 0,
                            CreateMemberId = createMemberId,
                            UpdateMemberId = createMemberId
                        }
                    };

                    if (!String.IsNullOrWhiteSpace(receiverSplitAddress.ErrMsg) || !String.IsNullOrWhiteSpace(senderSplitAddress.ErrMsg))
                    {
                        request.ErrMsg = String.IsNullOrWhiteSpace(receiverSplitAddress.ErrMsg) ? senderSplitAddress.ErrMsg : receiverSplitAddress.ErrMsg;
                        request.IsValidate = false;
                    }

                    if (!isCollectionAmountValidate)
                    {
                        request.ErrMsg = "請填入正確代收貨款";
                        request.IsValidate = false;
                    }
                }
                catch
                {
                    request.ErrMsg = "資料錯誤";
                    request.Row = r;
                }

                deliveryRequests.Add(request);
            }

            return "";
        }

        private CityAreaIdAddressEntity SplitFullAddress(string address, CityAreaEntity[] cityAreaEntities)
        {
            for (int c = 0; c < cityAreaEntities.Length; c++)   //檢查地址前綴是否正確
            {
                if (address.StartsWith(cityAreaEntities[c].City))
                {
                    for (int a = 0; a < cityAreaEntities[c].AreaEntities.Length; a++)
                    {
                        string matchedCityArea = cityAreaEntities[c].City + cityAreaEntities[c].AreaEntities[a].Area;
                        if (address.StartsWith(matchedCityArea))
                        {
                            return new CityAreaIdAddressEntity { CityAreaId = cityAreaEntities[c].AreaEntities[a].Id, Address = address.Replace(matchedCityArea, ""), ErrMsg = "" };
                        }
                    }
                }
            }

            return new CityAreaIdAddressEntity { CityAreaId = 0, Address = address, ErrMsg = "地址錯誤" };
        }

        private bool CheckExcelTitle(IXLWorksheet ws)
        {
            for (int i = 0; i < Titles.Length; i++)
            {
                if (ws.Cell(Titles[i].CellPosition).Value.ToString() != Titles[i].Content)
                {
                    return false;
                }
            }

            return true;
        }
    }
}