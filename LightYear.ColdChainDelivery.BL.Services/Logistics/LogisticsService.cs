﻿using LightYear.ColdChainDelivery.DA.ColdChainDeliveryDbReadOnly;
using LightYear.ColdChainDelivery.DA.Repositories.Logistics;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace LightYear.ColdChainDelivery.BL.Services.Logistics
{
    public class LogisticsService: ILogisticsService
    {
        public ILogisticsRepository LogisticsRepository { get; set; }
        public ILogisticsModifyRepository LogisticsModifyRepository { get; set; }

        public LogisticsService(ILogisticsRepository logisticsRepository, ILogisticsModifyRepository logisticsModifyRepository)
        {
            LogisticsRepository = logisticsRepository;
            LogisticsModifyRepository = logisticsModifyRepository;
        }

        public List<Logistic> GetLogistics()
        {
            return LogisticsRepository.GetLogistics();
        }

        public Logistic GetLogisticById(int id)
        {
            return LogisticsRepository.GetLogisticById(id);
        }

        public int Insert(DA.ColdChainDeliveryDb.Logistic logistic)
        {
            return LogisticsModifyRepository.Insert(logistic);   
        }

        public void Update(DA.ColdChainDeliveryDb.Logistic logistic) 
        {
            LogisticsModifyRepository.Update(logistic);
        }

        public void FakeDelete(DA.ColdChainDeliveryDb.Logistic logistic)
        {
            LogisticsModifyRepository.FakeDelete(logistic);
        }

        public string GetUniqueFileName(string fileName)
        {
            fileName = Path.GetFileName(fileName);
            return Path.GetFileNameWithoutExtension(fileName) //只有檔名
                      + "_"
                      + Guid.NewGuid().ToString().Substring(0, 4)
                      + Path.GetExtension(fileName); //.副檔名
        }

        public int? GetLogisticIdByCompanyCode(string companyCode)
        {
            return LogisticsRepository.GetLogisticIdByCompanyCode(companyCode);
        }
    }
}
