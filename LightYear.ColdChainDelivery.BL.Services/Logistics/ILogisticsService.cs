﻿using LightYear.ColdChainDelivery.DA.ColdChainDeliveryDbReadOnly;
using LightYear.ColdChainDelivery.DA.ColdChainDeliveryDb;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.ColdChainDelivery.BL.Services.Logistics
{
    public interface ILogisticsService
    {
        List<DA.ColdChainDeliveryDbReadOnly.Logistic> GetLogistics();

        int Insert(DA.ColdChainDeliveryDb.Logistic logistic);

        void Update(DA.ColdChainDeliveryDb.Logistic logistic);

        void FakeDelete(DA.ColdChainDeliveryDb.Logistic logistic);

        string GetUniqueFileName(string fileName);

        DA.ColdChainDeliveryDbReadOnly.Logistic GetLogisticById(int id);

        int? GetLogisticIdByCompanyCode(string companyCode);
    }
}
