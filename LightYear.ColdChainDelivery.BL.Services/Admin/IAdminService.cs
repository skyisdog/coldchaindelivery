﻿using LightYear.ColdChainDelivery.BL.BE.Account;
using LightYear.ColdChainDelivery.DA.ColdChainDeliveryDbReadOnly;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.ColdChainDelivery.BL.Services.Admin
{
    public interface IAdminService
    {
        List<FrontendMemberEntity> GetMembers();

        long Insert(DA.ColdChainDeliveryDb.Member member);

        void Update(DA.ColdChainDeliveryDb.Member member);

        List<Logistic> GetLogistics();
    }
}
