﻿using AutoMapper;
using LightYear.ColdChainDelivery.BL.BE.Account;
using LightYear.ColdChainDelivery.BL.Services.Account;
using LightYear.ColdChainDelivery.DA.ColdChainDeliveryDbReadOnly;
using LightYear.ColdChainDelivery.DA.Repositories.Logistics;
using LightYear.ColdChainDelivery.DA.Repositories.Member;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LightYear.ColdChainDelivery.BL.Services.Admin
{
    public class AdminService: IAdminService
    {
        IMemberRepository MemberRepository { get; set; }
        IMemberModifyRepository MemberModifyRepository { get; set; }
        ILogisticsRepository LogisticsRepository { get; set; }
        IAccountService AccountService { get; set; }
        IMapper Mapper { get; set; }

        public AdminService(IMemberRepository memberRepository, IMemberModifyRepository memberModifyRepository, ILogisticsRepository logisticsRepository, IAccountService accountService, IMapper mapper)
        {
            MemberRepository = memberRepository;
            MemberModifyRepository = memberModifyRepository;
            LogisticsRepository = logisticsRepository;
            AccountService = accountService;
            Mapper = mapper;
        }

        public List<FrontendMemberEntity> GetMembers()
        {
            List<DA.ColdChainDeliveryDb.Member> members = MemberModifyRepository.GetMembers();
            var memberEntities = Mapper.Map<List<FrontendMemberEntity>>(members);
            var logistics = LogisticsRepository.GetLogistics();

            foreach (var m in memberEntities)
            {
                AccountType accountType = (AccountType)Convert.ToInt32(m.AccountType);
                m.AccountTypeChinese = accountType.ToString();
                if (m.LogisticsId == null) m.LogisticsId = 0;
                m.LogisticsCompany = (logistics.Where(a => a.Id == m.LogisticsId).FirstOrDefault()?.Company) ?? "";
            }
            return memberEntities;
        }

        public long Insert (DA.ColdChainDeliveryDb.Member member)
        {
            var hashed = AccountService.CreateHashedPassword(member.Password);
            member.Password = hashed;
            return MemberModifyRepository.Insert(member);
        }

        public void Update(DA.ColdChainDeliveryDb.Member member)
        {
            MemberModifyRepository.Update(member);
        }

        public List<Logistic> GetLogistics()
        {
            var logistics = LogisticsRepository.GetLogistics();
            return logistics;
        }
    }
}
