﻿using LightYear.ColdChainDelivery.BL.BE.Account;
using LightYear.ColdChainDelivery.DA.ColdChainDeliveryDbReadOnly;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.ColdChainDelivery.BL.Services.Account
{
    public interface IAccountService
    {
        UserInfoEntity CheckLogin(string account, string password, string companyCode);
        string CreateHashedPassword(string password);
        Member GetMember(long id);
    }
}
