﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.ColdChainDelivery.DA.Repositories.Member;
using LightYear.ColdChainDelivery.BL.BE.Account;
using AutoMapper;
using System.Security.Cryptography;
using LightYear.ColdChainDelivery.DA.ColdChainDeliveryDbReadOnly;

namespace LightYear.ColdChainDelivery.BL.Services.Account
{
    public class AccountService : IAccountService
    {
        public IMemberRepository MemberRepository { get; set; }
        public IMapper Mapper { get; private set; }

        public AccountService(IMemberRepository memberRepository, IMapper mapper)
        {
            MemberRepository = memberRepository;
            Mapper = mapper;
        }

        public UserInfoEntity CheckLogin(string account, string password, string companyCode)
        {
            string hashed = CreateHashedPassword(password);

            var accountEntity = MemberRepository.GetMemberByAccountAndHashedPassword(account, hashed, companyCode);
            
            if (accountEntity == null)
            {
                return null;
            }

            UserInfoEntity userInfoEntity = Mapper.Map<UserInfoEntity>(accountEntity);
            
            return userInfoEntity;
        }

        public string CreateHashedPassword(string password)
        {
            SHA512 sHA512 = SHA512.Create("SHA512");
            byte[] source = Encoding.Default.GetBytes(password);
            byte[] output = sHA512.ComputeHash(source);
            string result = Convert.ToBase64String(output);
            return result;
        }

        public Member GetMember(long id)
        {
            return MemberRepository.GetMember(id);
        }

    }
}
