﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.ColdChainDelivery.BL.BE.StudentTest;

namespace LightYear.ColdChainDelivery.BL.Services.StudentTest
{
    public interface IStudentService
    {
        List<PersonEntity> GetAllStudents();

        PersonEntity GetStudent(int id);

        void SaveStudent(PersonEntity personEntity);

        void DeleteStudent(int id);

    }
}
