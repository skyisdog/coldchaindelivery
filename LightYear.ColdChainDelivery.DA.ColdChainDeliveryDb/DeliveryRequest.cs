// <auto-generated>
// ReSharper disable All

using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace LightYear.ColdChainDelivery.DA.ColdChainDeliveryDb
{
    // delivery_request
    public class DeliveryRequest
    {
        public long Id { get; set; } // id (Primary key)
        public long? ProjectId { get; set; } // project_id
        public string DeliveryNumber { get; set; } // delivery_number (length: 20)
        public string OrderNumber { get; set; } // order_number (length: 20)
        public string SenderName { get; set; } // sender_name (length: 100)
        public long? SenderZipId { get; set; } // sender_zip_id
        public string SenderAddress { get; set; } // sender_address (length: 100)
        public string ReceiverName { get; set; } // receiver_name (length: 100)
        public string ReceiverTel1 { get; set; } // receiver_tel1 (length: 100)
        public string ReceiverTel2 { get; set; } // receiver_tel2 (length: 100)
        public long? ReceiverZipId { get; set; } // receiver_zip_id
        public string ReceiverAddress { get; set; } // receiver_address (length: 100)

        /// <summary>
        /// 代收金額
        /// </summary>
        public int CollectionAmount { get; set; } // collection_amount
        public long? CreateMemberId { get; set; } // create_member_id
        public DateTime? DeleteAt { get; set; } // delete_at
        public long? UpdateMemberId { get; set; } // update_member_id

        // Foreign keys

        /// <summary>
        /// Parent Member pointed by [delivery_request].([CreateMemberId]) (fk_member)
        /// </summary>
        public virtual Member CreateMember { get; set; } // fk_member

        /// <summary>
        /// Parent Member pointed by [delivery_request].([UpdateMemberId]) (fk_update_member_id)
        /// </summary>
        public virtual Member UpdateMember { get; set; } // fk_update_member_id

        /// <summary>
        /// Parent PostCityArea pointed by [delivery_request].([ReceiverZipId]) (receiver_zip)
        /// </summary>
        public virtual PostCityArea ReceiverZip { get; set; } // receiver_zip

        /// <summary>
        /// Parent PostCityArea pointed by [delivery_request].([SenderZipId]) (sender_zip)
        /// </summary>
        public virtual PostCityArea SenderZip { get; set; } // sender_zip

        /// <summary>
        /// Parent Project pointed by [delivery_request].([ProjectId]) (fk_project)
        /// </summary>
        public virtual Project Project { get; set; } // fk_project
    }

}
// </auto-generated>
