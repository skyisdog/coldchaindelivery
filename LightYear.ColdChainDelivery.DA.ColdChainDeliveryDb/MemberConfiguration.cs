// <auto-generated>
// ReSharper disable All

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LightYear.ColdChainDelivery.DA.ColdChainDeliveryDb
{
    // member
    public class MemberConfiguration : IEntityTypeConfiguration<Member>
    {
        public void Configure(EntityTypeBuilder<Member> builder)
        {
            builder.ToTable("member", "public");
            builder.HasKey(x => x.Id).HasName("member_pkey");

            builder.Property(x => x.Id).HasColumnName(@"id").HasColumnType("bigint").IsRequired().ValueGeneratedOnAdd().UseIdentityColumn();
            builder.Property(x => x.Account).HasColumnName(@"account").HasColumnType("character varying(50)").IsRequired().HasMaxLength(50);
            builder.Property(x => x.Password).HasColumnName(@"password").HasColumnType("character varying(255)").IsRequired(false).HasMaxLength(255);
            builder.Property(x => x.UserName).HasColumnName(@"user_name").HasColumnType("character varying(100)").IsRequired().HasMaxLength(100);
            builder.Property(x => x.UserEmail).HasColumnName(@"user_email").HasColumnType("character varying(100)").IsRequired().HasMaxLength(100);
            builder.Property(x => x.UserPhone).HasColumnName(@"user_phone").HasColumnType("character varying(100)").IsRequired(false).HasMaxLength(100);
            builder.Property(x => x.Memo).HasColumnName(@"memo").HasColumnType("text").IsRequired(false).IsUnicode(false);
            builder.Property(x => x.Sex).HasColumnName(@"sex").HasColumnType("integer").IsRequired(false);
            builder.Property(x => x.CreatedAt).HasColumnName(@"created_at").HasColumnType("timestamp without time zone").IsRequired();
            builder.Property(x => x.UpdatedAt).HasColumnName(@"updated_at").HasColumnType("timestamp without time zone").IsRequired();
            builder.Property(x => x.ActiveFlag).HasColumnName(@"active_flag").HasColumnType("integer").IsRequired();
            builder.Property(x => x.DeletedAt).HasColumnName(@"deleted_at").HasColumnType("timestamp without time zone").IsRequired(false);
            builder.Property(x => x.IsDeleted).HasColumnName(@"is_deleted").HasColumnType("integer").IsRequired();
            builder.Property(x => x.Birthday).HasColumnName(@"birthday").HasColumnType("timestamp without time zone").IsRequired(false);
            builder.Property(x => x.AccountType).HasColumnName(@"account_type").HasColumnType("character varying(2)").IsRequired(false).HasMaxLength(2);
            builder.Property(x => x.LogisticsId).HasColumnName(@"logistics_id").HasColumnType("integer").IsRequired(false);
            builder.Property(x => x.CustomerCompanyName).HasColumnName(@"customer_company_name").HasColumnType("character varying(20)").IsRequired(false).HasMaxLength(20);
            builder.Property(x => x.CustomerCompanyTaxId).HasColumnName(@"customer_company_tax_id").HasColumnType("character varying(10)").IsRequired(false).HasMaxLength(10);
            builder.Property(x => x.CustomerCompanyAddress).HasColumnName(@"customer_company_address").HasColumnType("character varying(60)").IsRequired(false).HasMaxLength(60);

            builder.HasIndex(x => x.Account).HasName("member_account");
        }
    }

}
// </auto-generated>
