using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Autofac;
using Autofac.Configuration;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using LightYear.ColdChainDelivery.BL.Services.StudentTest;
using LightYear.ColdChainDelivery.DA.ColdChainDeliveryDb;
using LightYear.ColdChainDelivery.DA.ColdChainDeliveryDbReadOnly;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using reCAPTCHA.AspNetCore;

namespace LightYear.ColdChainDelivery.WebSite
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();

            services.AddDbContext<ColdChainDeliveryDbContext>(options =>
                options.UseNpgsql(Configuration.GetConnectionString("ColdChainDeliveryDbContext")));

            services.AddDbContext<ColdChainDeliveryReadOnlyDbContext>(options =>
                options.UseNpgsql(Configuration.GetConnectionString("ColdChainDeliveryReadOnlyDbContext")));

            //從組態讀取登入逾時設定
            double LoginExpireMinute = this.Configuration.GetValue<double>("LoginExpireMinute");
            //註冊 CookieAuthentication，Scheme必填
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme).AddCookie(option =>
            {
                //瀏覽器會限制cookie 只能經由HTTP(S) 協定來存取
                option.Cookie.HttpOnly = true;
                //或許要從組態檔讀取，後續再修改
                option.LoginPath = new PathString("/Account/Login");//登入頁
                option.LogoutPath = new PathString("/Account/Logout");//登出Action
                //用戶頁面停留太久，登入逾期，或Controller中用戶登入時機點也可以設定↓
                option.ExpireTimeSpan = TimeSpan.FromMinutes(LoginExpireMinute);//沒給預設14天
            });

            services.AddDistributedMemoryCache();
            services.AddSession();

            services.AddControllersWithViews().AddRazorRuntimeCompilation();

            services.AddMvc().AddXmlSerializerFormatters();

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.AddHttpContextAccessor();

            services.AddRazorPages().AddRazorRuntimeCompilation();

            services.AddRecaptcha(Configuration.GetSection("RecaptchaSettings"));

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseDefaultFiles();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseSession();
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }

        public void ConfigureContainer(ContainerBuilder builder)
        {
            builder.RegisterModule(new AutofacModule());
        }
    }

    public class AutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {

            //// Read autofac settings from config
            var autofacConfig = new ConfigurationBuilder();
            autofacConfig.AddJsonFile("autofac.json");

            var configRoot = autofacConfig.Build();

            // Register the ConfigurationModule with Autofac.
            var module = new ConfigurationModule(configRoot);
            builder.RegisterModule(module);

        }
    }
}
