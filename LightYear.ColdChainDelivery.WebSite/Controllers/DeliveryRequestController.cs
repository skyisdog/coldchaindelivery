﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LightYear.ColdChainDelivery.BL.BE.DeliveryRequest;
using LightYear.ColdChainDelivery.BL.Services.DeliveryRequest;
using Newtonsoft.Json;
using System.Web;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;
using LightYear.ColdChainDelivery.BL.BE.Account;
using Microsoft.AspNetCore.Authorization;
using LightYear.ColdChainDelivery.BL.Services.Logistics;

namespace LightYear.ColdChainDelivery.WebSite.Controllers
{
    public class DeliveryRequestController : Controller
    {
        public IDeliveryRequestService DeliveryRequestService;
        public ILogisticsService LogisticsService;
        public DeliveryRequestController(IDeliveryRequestService deliveryRequestService, ILogisticsService logisticsService)
        {
            DeliveryRequestService = deliveryRequestService;
            LogisticsService = logisticsService;
        }

        public IActionResult Index()
        {
            return View();
        }

        [Authorize]
        [Route("store/{companyCode}/[action]")]
        public IActionResult DeliveryRequest()
        {
            UserInfoEntity userInfo = GetLoginInfo();
            CreateDeliveryRequestEntity entity = DeliveryRequestService.GetCreateOrderDafault(userInfo.Id);
            return View(entity);
        }

        [Route("store/{companyCode}/[action]")]
        public IActionResult GetCityArea()
        {
            var data = DeliveryRequestService.PostCityAreas();
            return Json(data);
        }

        [Route("store/{companyCode}/[action]")]
        public IActionResult SaveDeliveryRequest(CreateDeliveryRequestEntity createDeliveryRequestEntity, string companyCode)
        {
            UserInfoEntity userInfo = GetLoginInfo();
            createDeliveryRequestEntity.CreateMemberId = userInfo.Id;

            long id = DeliveryRequestService.Insert(createDeliveryRequestEntity, userInfo.LogisticId);
            return Redirect("/store/" + companyCode + "/DeliveryRequestFinished?id=" + id.ToString());
        }

        [Authorize]
        [Route("store/{companyCode}/[action]")]
        public IActionResult DeliveryRequestFinished(long id, string companyCode)
        {
            if (id == 0)
                return RedirectToAction("/store/" + companyCode + "/DeliveryRequest");

            CreateDeliveryRequestEntity entity = DeliveryRequestService.GetDeliveryRequestEntity(id);

            return View(entity);
        }

        [Authorize]
        [Route("store/{companyCode}/[action]")]
        public IActionResult GetMembers()
        {
            UserInfoEntity userInfo = GetLoginInfo();
            bool isAdmin = IsUserAdmin(userInfo);
            if (isAdmin)
            {
                List<BL.BE.DeliveryRequest.MemberEntity> members = DeliveryRequestService.GetCustomerMemberList();
                return Json(members);
            }
            else 
            {
                return Json(new BL.BE.DeliveryRequest.MemberEntity[1] { new BL.BE.DeliveryRequest.MemberEntity { Id = userInfo.Id, Name = userInfo.UserName } });
            }
        }

        [Authorize]
        [Route("store/{companyCode}/[action]")]
        public IActionResult SearchDeliveryRequests(DeliveryRequestInputEntity inputEntity, string companyCode)
        {
            int logisticId = LogisticsService.GetLogisticIdByCompanyCode(companyCode) ?? 0;
            DeliveryRequestEntity[] deliveryRequests = DeliveryRequestService.SearchDeliveryRequest(inputEntity, logisticId);
            string deliveryRequestInJson = JsonConvert.SerializeObject(deliveryRequests).Replace("\"", "\\\"");
            return View("SearchPage", deliveryRequestInJson);
        }

        [Route("store/{companyCode}/[action]")]
        public IActionResult GetDefaultForSearchFilter()
        {
            DateTime now = DateTime.Now;
            DeliveryRequestInputEntity defaultValue = new DeliveryRequestInputEntity {
                ProjectNo = "",
                DeliveryNumber = "",
                OrderNumber = "",
                ReceiverName = "",
                CreateDateStart = now,
                CreateDateEnd = now,
                DeliveryDateStart = now,
                DeliveryDateEnd = now
            };
            string defaultValueInJson = JsonConvert.SerializeObject(defaultValue);
            return Content(defaultValueInJson);
        }

        [Authorize]
        [Route("store/{companyCode}/[action]")]
        public IActionResult SearchPage()
        {
            return View();
        }

        [Route("store/{companyCode}/[action]")]
        public IActionResult UpdateDeliveryRequest(DeliveryRequestEntity item)
        {
            UserInfoEntity userInfo = GetLoginInfo();
            DeliveryRequestEntity request = DeliveryRequestService.PartialEdit(item, userInfo.Id);
            return Content(JsonConvert.SerializeObject(request));
        }

        [Route("store/{companyCode}/[action]")]
        public IActionResult UpdateDeliveryRequestWithSenderInfo(DeliveryRequestEntity item)      //與上方的差別是，此方法會更新託運單的寄件人資訊，且不考慮是否已經取件(用於批次匯入的結果頁)
        {
            UserInfoEntity userInfo = GetLoginInfo();
            DeliveryRequestEntity request = DeliveryRequestService.PartialEditWithSenderInfo(item, userInfo.Id);
            return Content(JsonConvert.SerializeObject(request));
        }

        [Route("store/{companyCode}/[action]")]
        public IActionResult DeleteDeliveryRequest(DeliveryRequestEntity item)
        {
            UserInfoEntity userInfo = GetLoginInfo();
            long returnValue = DeliveryRequestService.SoftDelete(item.Id, userInfo.Id);
            return Content(JsonConvert.SerializeObject(item));
        }

        [Route("store/{companyCode}/[action]")]
        public IActionResult PrintPaper(long[] ids)
        {
            return Content("");
        }

        [Authorize]
        [Route("store/{companyCode}/[action]")]
        public IActionResult BatchDeliveryRequests()
        {
            List<BL.BE.DeliveryRequest.MemberEntity> members = new List<BL.BE.DeliveryRequest.MemberEntity>();
            UserInfoEntity userInfo = GetLoginInfo();
            bool isAdmin = IsUserAdmin(userInfo);
            if (isAdmin)
            {
                members = DeliveryRequestService.GetCustomerMemberList();
            }
            
            return View("BatchDeliveryRequests",JsonConvert.SerializeObject(members));
        }

        [HttpPost]
        [Route("store/{companyCode}/[action]")]
        public IActionResult BatchSaveDeliveryRequests(UploadExcelModel model, string companyCode)
        {
            int logisticId = LogisticsService.GetLogisticIdByCompanyCode(companyCode) ?? 0;
            var file = model.File;

            UserInfoEntity userInfo = GetLoginInfo();
            bool isAdmin = IsUserAdmin(userInfo);
            if (!isAdmin)       //非管理者，強制改為登入memberId
            {
                model.MemberId = userInfo.Id;
            }
            
            BatchDeliveryRequestResultEntity resultEntity = DeliveryRequestService.SaveBatchDeliveryRequests(file, model.MemberId, userInfo.Id, logisticId);
            string resultEntityInJson = JsonConvert.SerializeObject(resultEntity);
            return Content(resultEntityInJson);
        }

        [Route("store/{companyCode}/[action]")]
        public IActionResult DownloadBatchCreateDeliveryRequestDefaultData()
        {
            byte[] fileInBytes = DeliveryRequestService.CreateDefaultBatchCreateDeliveryRequestsFile();

            var cd = new System.Net.Mime.ContentDisposition
            {
                FileName = "空白批次託運單.xlsx"
            };
            Response.Headers.Add("Content-Disposition", cd.ToString());

            return File(fileInBytes, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            string companyCode = context.RouteData.Values["companyCode"].ToString();
            int? logisticId = LogisticsService.GetLogisticIdByCompanyCode(companyCode);
            if (logisticId == null)
            {
                context.Result = new NotFoundResult();
                return;
            }

            base.OnActionExecuting(context);
        }

        private UserInfoEntity GetLoginInfo()
        {
            UserInfoEntity userInfo = JsonConvert.DeserializeObject<UserInfoEntity>(HttpContext.User.Claims.Where(u => u.Type == "UserInfo").FirstOrDefault()?.Value);
            return userInfo;
        }

        private bool IsUserAdmin(UserInfoEntity userInfo)
        {
            return userInfo.AccountType == "1";
        }
    }
}
