﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using LightYear.ColdChainDelivery.BL.BE.Account;
using LightYear.ColdChainDelivery.BL.Services.Account;
using LightYear.ColdChainDelivery.BL.Services.Logistics;
using LightYear.ColdChainDelivery.DA.ColdChainDeliveryDb;
using LightYear.ColdChainDelivery.DA.ColdChainDeliveryDbReadOnly;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace LightYear.ColdChainDelivery.WebSite.Controllers
{
    public class LogisticsController : Controller
    {
        public ILogisticsService LogisticsService { get; set; }
        private readonly IConfiguration Configuration;
        IAccountService AccountService { get; set; }

        public LogisticsController(ILogisticsService logisticsService, IConfiguration configuration, IAccountService accountService)
        {
            LogisticsService = logisticsService;
            Configuration = configuration;
            AccountService = accountService;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult GetLogistics()
        {
            var data = LogisticsService.GetLogistics();
            var output = JsonConvert.SerializeObject(data);
            return Content(output);
        }

        public IActionResult GetActiveLogistics()
        {
            List<DA.ColdChainDeliveryDbReadOnly.Logistic> data = LogisticsService.GetLogistics().Where(a => a.Active.Equals(1)).ToList();

            var claim = HttpContext.User.Claims.FirstOrDefault(a => a.Type == "UserInfo");

            if (claim != null)
            {
                var userInfoString = claim.Value;
                UserInfoEntity userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoString);
                var member = AccountService.GetMember(userInfoEntity.Id);
                var name = LogisticsService.GetLogisticById(member.LogisticsId ?? 0);
                if (name != null)
                    data = data.Where(a => a.Id.Equals(member.LogisticsId ?? 0)).ToList();
            }
            return Json(data);
        }

        public async Task<IActionResult> Insert (DA.ColdChainDeliveryDb.Logistic logistic, IFormFile file)
        {
            string fullPath = "";

            if (file != null && file.Length > 0)
            {
                var savePath = Configuration.GetValue<string>("UploadLogo");

                string uniqueFileName = LogisticsService.GetUniqueFileName(file.FileName);

                fullPath = string.Format("{0}/{1}", savePath, uniqueFileName);

                using (FileStream stream = new FileStream(fullPath, FileMode.Append))
                {
                    await file.CopyToAsync(stream);
                }
            }

            if (logistic.Id > 1) // update
            {
                if(!string.IsNullOrEmpty(fullPath))
                    logistic.Logo = fullPath.Substring(7);
                LogisticsService.Update(logistic);
            }
            else //insert
            {
                logistic.Logo = fullPath.Substring(7);
                var id = LogisticsService.Insert(logistic);
                logistic.Id = id;
            }

            return View("index");
        }

        public IActionResult FakeDelete(string json)
        {
            List<DA.ColdChainDeliveryDb.Logistic> logistic = JsonConvert.DeserializeObject<List<DA.ColdChainDeliveryDb.Logistic>>(json);
            LogisticsService.FakeDelete(logistic[0]);
            var output = JsonConvert.SerializeObject(logistic[0]);
            return Content(output);
        }
    }
}