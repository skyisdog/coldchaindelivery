﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using LightYear.ColdChainDelivery.BL.BE.Account;
using LightYear.ColdChainDelivery.BL.Services.Account;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using reCAPTCHA.AspNetCore.Attributes;
using Microsoft.Extensions.Options;
using reCAPTCHA.AspNetCore;

namespace LightYear.ColdChainDelivery.WebSite.Controllers
{
    public class AccountController : Controller
    {
        IAccountService AccountService { get; set; }
        IConfiguration Configuration { get; set; }
        private readonly IRecaptchaService _recaptcha;

        public AccountController(IAccountService accountService, IConfiguration configuration, IRecaptchaService recaptcha)
        {
            AccountService = accountService;
            Configuration = configuration;
            _recaptcha = recaptcha;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Login(string ReturnUrl = "")
        {
            ViewBag.ReturnUrl = ReturnUrl;
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> AfterLogin(LoginEntity loginEntity)
        {
            UserInfoEntity userInfoEntity = AccountService.CheckLogin(loginEntity.Account, loginEntity.Password, loginEntity.CompanyCode);

            var recaptchaResult = await _recaptcha.Validate(loginEntity.GoogleToken);

            if (!recaptchaResult.success || recaptchaResult.score < 0.08)
            {
                TempData["Message"] = "您已被判讀為機器人。";
                return View("Login");
            }
            else
            {
                if (userInfoEntity == null)
                {
                    TempData["Message"] = "帳號密碼錯誤!!";

                    if (String.IsNullOrWhiteSpace(loginEntity.CompanyCode))
                    {
                        return View("Login");
                    }
                    else
                    {
                        return Redirect(string.Format("~/store/{0}/Login", loginEntity.CompanyCode));
                    }
                }
            }

            userInfoEntity.CookieExpiration = DateTime.Now.AddDays(1);

            var loginData = JsonConvert.SerializeObject(userInfoEntity);

            Claim[] claims = new[] { new Claim("Account", userInfoEntity.Account), new Claim("UserInfo", loginData) };
            ClaimsIdentity claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
            ClaimsPrincipal claimsPrincipal = new ClaimsPrincipal(claimsIdentity);

            await HttpContext.SignInAsync(claimsPrincipal,
                new AuthenticationProperties()
                {
                    IsPersistent = false, //IsPersistent = false，瀏覽器關閉即刻登出
                                          //用戶頁面停留太久，逾期時間，在此設定的話會覆蓋Startup.cs裡的逾期設定
                });

            //寫入cookie給跨網域使用
            //先刪除原有的cookie
            if (Request.Cookies["OtherLogin"] != null)
            {
                Response.Cookies.Delete("OtherLogin");
            }

            CookieOptions options = new CookieOptions
            {
                Expires = DateTime.Now.AddDays(1),
                Domain = Configuration.GetValue<string>("CookieDomain")
            };

            Response.Cookies.Append("OtherLogin", loginData, options);

            //加上 Url.IsLocalUrl 防止Open Redirect漏洞
            if (!string.IsNullOrWhiteSpace(loginEntity.CompanyCode))
            {
                return Redirect(string.Format("~/store/{0}/Home", loginEntity.CompanyCode));
            }
            else if (!string.IsNullOrEmpty(loginEntity.ReturnUrl) && (Url.IsLocalUrl(loginEntity.ReturnUrl) || loginEntity.ReturnUrl.IndexOf(options.Domain) > 0))
            {
                return Redirect(loginEntity.ReturnUrl); //導到原始要求網址
            }
            else
            {
                return RedirectToAction("Index", "Home"); //到登入後的第一頁
            }
        }

        public async Task<IActionResult> LogOut()
        {
            await HttpContext.SignOutAsync();

            CookieOptions options = new CookieOptions
            {
                Expires = DateTime.Now.AddDays(-1),
                Domain = Configuration.GetValue<string>("CookieDomain")
            };

            Response.Cookies.Append("OtherLogin","",options);
            
            return RedirectToAction("Login", "Account");
        }
    }
}