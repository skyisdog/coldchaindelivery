﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LightYear.ColdChainDelivery.DA.ColdChainDeliveryDb;
using LightYear.ColdChainDelivery.BL.Services.StudentTest;
using LightYear.ColdChainDelivery.BL.BE.StudentTest;
using Newtonsoft.Json;

namespace LightYear.ColdChainDelivery.WebSite.Controllers
{
    public class TestStudentController : Controller
    {
        IStudentService StudentService;

        public TestStudentController(IStudentService studentService)
        {
            StudentService = studentService;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult GetStudent(int id)
        {

            PersonEntity student = StudentService.GetStudent(id);

            return View(student);
        }

        public IActionResult Students()
        {
            return View();
        }

        public IActionResult GetAll(string callback = "callback")
        {
            List<PersonEntity> personEntities = this.StudentService.GetAllStudents();

            string students = JsonConvert.SerializeObject(personEntities);

            string output = string.Format("{0}({1})", callback, students);

            return Content(output);
        }

        public IActionResult Create(string models,  string callback = "callback")
        {

            List<PersonEntity> personEntities = JsonConvert.DeserializeObject<List<PersonEntity>>(models);

            this.StudentService.SaveStudent(personEntities[0]);

            string output = string.Format("{0}({1})", callback, models);

            return Content(output);
        }

        public IActionResult Destroy(string models, string callback = "callback")
        {

            List<PersonEntity> personEntities = JsonConvert.DeserializeObject<List<PersonEntity>>(models);

            int deleteId = personEntities[0].Id ?? 0;

            this.StudentService.DeleteStudent(deleteId);

            string output = string.Format("{0}({1})", callback, models);

            return Content(output);
        }
    }
}