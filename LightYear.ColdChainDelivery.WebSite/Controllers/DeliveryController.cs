﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LightYear.ColdChainDelivery.BL.Services.DeliveryRequest;
using System.Text.Json;
using LightYear.ColdChainDelivery.BL.BE.DeliveryRequest;
using Microsoft.AspNetCore.Authorization;
using LightYear.ColdChainDelivery.BL.BE.Account;
using Newtonsoft.Json;
using LightYear.ColdChainDelivery.BL.Services.Logistics;
using Microsoft.AspNetCore.Mvc.Filters;

namespace LightYear.ColdChainDelivery.WebSite.Controllers
{
    public class DeliveryController : Controller
    {
        IDeliveryRequestService DeliveryRequestService { get; set; }
        ILogisticsService LogisticsService { get; set; }
        public DeliveryController(IDeliveryRequestService deliveryRequestService, ILogisticsService logisticsService)
        {
            DeliveryRequestService = deliveryRequestService;
            LogisticsService = logisticsService;
        }

        [Route("store/{companyCode}/[action]")]
        public IActionResult Login(string companyCode)
        {
            ViewBag.ReturnUrl = "";
            ViewBag.CompanyCode = companyCode;
            return View();
        }

        [Route("store/{companyCode}/[action]")]
        public IActionResult Home(string companyCode)
        {
            string url = DeliveryRequestService.GetLogoUrlByLogisticsCode(companyCode);
            UserInfoEntity userInfo = GetLoginInfo();
            IndexParamsEntity indexParams = new IndexParamsEntity
            {
                Url = url,
                IsLogin = userInfo == null ? false : true
            };
            return View("Index", indexParams);
        }

        [Route("store/{companyCode}/[action]")]
        public IActionResult GetSimpleOrderDetail(string[] inputNumbers, string companyCode)
        {
            int logisticId = LogisticsService.GetLogisticIdByCompanyCode(companyCode) ?? 0;
            inputNumbers = inputNumbers.Where(i => i != null).ToArray();
            return Json(DeliveryRequestService.GetSimpleDeliveryRequestByDeliveryNumbers(inputNumbers, logisticId));
        }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            string companyCode = context.RouteData.Values["companyCode"].ToString();
            int? logisticId = LogisticsService.GetLogisticIdByCompanyCode(companyCode);
            if (logisticId == null)
            {
                context.Result = new NotFoundResult();
                return;
            }

            base.OnActionExecuting(context);
        }

        private UserInfoEntity GetLoginInfo()
        {
            var claimForUser = HttpContext.User.Claims.Where(u => u.Type == "UserInfo").FirstOrDefault();
            if (claimForUser != null)
            {
                UserInfoEntity userInfo = JsonConvert.DeserializeObject<UserInfoEntity>(HttpContext.User.Claims.Where(u => u.Type == "UserInfo").FirstOrDefault().Value);
                return userInfo;
            }

            return null;
        }
    }
}
