﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using LightYear.ColdChainDelivery.BL.BE.Account;
using LightYear.ColdChainDelivery.BL.Services.Account;
using LightYear.ColdChainDelivery.BL.Services.Admin;
using LightYear.ColdChainDelivery.BL.Services.Logistics;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace LightYear.ColdChainDelivery.WebSite.Controllers
{
    public class AdminController : Controller
    {
        IAdminService AdminService { get; set; }
        IMapper Mapper { get; set; }
        IAccountService AccountService { get; set; }
        public ILogisticsService LogisticsService { get; set; }

        public AdminController(IAdminService adminService, IMapper mapper, IAccountService accountService, ILogisticsService logisticsService)
        {
            AdminService = adminService;
            Mapper = mapper;
            AccountService = accountService;
            LogisticsService = logisticsService;
        }

        public IActionResult Index()
        {
            if (HttpContext.User.Identity.IsAuthenticated == false)
            {
                return RedirectToAction("Login", "Account");
            }

            //var claim = HttpContext.User.Claims.FirstOrDefault(a => a.Type == "UserInfo");

            //if (claim != null)
            //{
            //    var userInfoString = claim.Value;
            //    UserInfoEntity userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoString);
            //    var member = AccountService.GetMember(userInfoEntity.Id);
            //    var name = LogisticsService.GetLogisticById(member.LogisticsId ?? 0);
            //    if (name != null)
            //        ViewBag.UserLogisticName = name.Company;
            //}
               
            return View();
        }

        public IActionResult GetMembers()
        {
            List<FrontendMemberEntity> output = new List<FrontendMemberEntity>();
            var claim = HttpContext.User.Claims.FirstOrDefault(a => a.Type == "UserInfo");
            if (claim == null)
            {
                output = AdminService.GetMembers();
            }
            else
            {
                var userInfoString = claim.Value;
                UserInfoEntity userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoString);
                var member = AccountService.GetMember(userInfoEntity.Id);
                if (member.LogisticsId.HasValue)
                {
                    output = AdminService.GetMembers().Where(a => a.LogisticsId == member.LogisticsId).ToList();
                }
                else
                {
                    output = AdminService.GetMembers();
                }
            }

            string json = JsonConvert.SerializeObject(output);
            return Content(json);
        }

        public IActionResult Insert(MemberEntity memberEntity)
        {
            if (memberEntity.CustomerCompanyTaxId.Length != 8)
            {
                TempData["Error"] = "客戶統編請輸入8碼數字。";
                return View("index");
            }

            memberEntity.LogisticsId = AdminService.GetLogistics().Where(a => a.Company.Equals(memberEntity.LogisticsCompany)).FirstOrDefault()?.Id;
            var accuntType = (AccountType)(Enum.Parse(typeof(AccountType), memberEntity.AccountTypeChinese));
            memberEntity.AccountType = Convert.ToInt32(accuntType).ToString(); //數字

            DA.ColdChainDeliveryDb.Member member = Mapper.Map<DA.ColdChainDeliveryDb.Member>(memberEntity);
            var id = AdminService.Insert(member);
            member.Id = id;
            var output = JsonConvert.SerializeObject(member);
            return View("index");
        }

        public IActionResult Update(string json)
        {
            List<FrontendMemberEntity> memberEntities = JsonConvert.DeserializeObject<List<FrontendMemberEntity>>(json);
            memberEntities[0].LogisticsId = AdminService.GetLogistics().Where(a => a.Company.Equals(memberEntities[0].LogisticsCompany)).FirstOrDefault()?.Id;
            var accuntType = (AccountType)(Enum.Parse(typeof(AccountType), memberEntities[0].AccountTypeChinese));
            memberEntities[0].AccountType = Convert.ToInt32(accuntType).ToString(); //數字

            List<DA.ColdChainDeliveryDb.Member> members = Mapper.Map<List<DA.ColdChainDeliveryDb.Member>>(memberEntities);
            AdminService.Update(members[0]);
           
            return Content(json);
        }
    }
}